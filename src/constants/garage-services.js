export default [
    {
        id: 1,
        name: 'Scheduled service'
    },
    {
        id: 2,
        name: 'Jumpstart and Diagnostics'
    },
    {
        id: 3,
        name: 'AC services'
    },
    {
        id: 4,
        name: 'Tyre replacement'
    },
    {
        id: 5,
        name: 'Tyre refitting'
    },
    {
        id: 6,
        name: 'Cleaning and Detailing',
        sub_name: 'Interior Sanitization'
    },
    {
        id: 7,
        name: 'Cleaning and Detailing',
        sub_name: 'Exterior polishing and rubbing'
    },
    {
        id: 8,
        name: 'Cleaning and Detailing',
        sub_name: 'Interior dry cleaning'
    },
    {
        id: 9,
        name: 'Cleaning and Detailing',
        sub_name: 'Washing'
    },
    {
        id: 10,
        name: 'Cleaning and Detailing',
        sub_name: 'Ceramic coating'
    },
    {
        id: 11,
        name: 'Cleaning and Detailing',
        sub_name: 'Teflon coating'
    },
    {
        id: 12,
        name: 'Cleaning and Detailing',
        sub_name: 'Interior and Exterior complete cleaning'
    },
    {
        id: 13,
        name: 'Sunroof services',
        sub_name: 'Sunroof repair'
    },
    {
        id: 14,
        name: 'Sunroof services',
        sub_name: 'Sunroof motor replacement'
    },
    {
        id: 15,
        name: 'Sunroof services',
        sub_name: 'Sunroof replace'
    },
    {
        id: 16,
        name: 'Sunroof services',
        sub_name: 'Sunroof service'
    },
    {
        id: 17,
        name: 'Denting Painting',
        sub_name: 'Full body dent paint'
    },
    {
        id: 18,
        name: 'Denting Painting',
        sub_name: 'RHS Fender paint'
    },
    {
        id: 19,
        name: 'Denting Painting',
        sub_name: 'LHS Fender paint'
    },
    {
        id: 20,
        name: 'Denting Painting',
        sub_name: 'LHS Rear Door paint'
    },
    {
        id: 21,
        name: 'Denting Painting',
        sub_name: 'LHS Front Door paint'
    },
    {
        id: 22,
        name: 'Denting Painting',
        sub_name: 'RHS Rear Door paint'
    },
    {
        id: 23,
        name: 'Denting Painting',
        sub_name: 'RHS Front Door paint'
    },
    {
        id: 24,
        name: 'Denting Painting',
        sub_name: 'RHS Quarter Panel paint'
    },
    {
        id: 25,
        name: 'Denting Painting',
        sub_name: 'LHS Quarter Panel paint'
    },
    {
        id: 26,
        name: 'Denting Painting',
        sub_name: 'RHS Running Board paint'
    },
    {
        id: 27,
        name: 'Denting Painting',
        sub_name: 'LHS Running Board paint'
    },
    {
        id: 28,
        name: 'Denting Painting',
        sub_name: 'Rear bumper paint'
    },
    {
        id: 29,
        name: 'Denting Painting',
        sub_name: 'Front Bumper paint'
    },
    {
        id: 30,
        name: 'Denting Painting',
        sub_name: 'Bonnet paint'
    },
    {
        id: 31,
        name: 'Denting Painting',
        sub_name: 'Boot paint'
    },
    {
        id: 32,
        name: 'Wheel Services',
        sub_name: 'Wheel alignment'
    },
    {
        id: 33,
        name: 'Wheel Services',
        sub_name: 'Wheel balancing'
    },

    {
        id: 34,
        name: 'Pre purchase car inspection'
    },
    {
        id: 35,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield replacement'
    },
    {
        id: 36,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Electric window faults'
    },
    {
        id: 37,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Window replacement'
    },
    {
        id: 38,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield wipers fluid change'
    },
    {
        id: 39,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield wipers replace'
    },
    {
        id: 40,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield wipers repair'
    },
    {
        id: 41,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield wiper motor replace'
    },
    {
        id: 42,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield wiper switch replacement'
    },
    {
        id: 43,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield wiper pump replacement'
    },
    {
        id: 44,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Rear windshield replacement'
    },
    {
        id: 45,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Window repair'
    },
    {
        id: 46,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Windshield repair'
    },
    {
        id: 47,
        name: 'Windows, Windshield and Mirrors',
        sub_name: 'Any other we missed?'
    },
    {
        id: 48,
        name: 'Unable to figure out?',
        sub_name: 'Car is not starting'
    },
    {
        id: 49,
        name: 'Unable to figure out?',
        sub_name: 'Smoking, Strange smell'
    },
    {
        id: 50,
        name: 'Unable to figure out?',
        sub_name: 'Banging or Knocking noise'
    },
    {
        id: 51,
        name: 'Unable to figure out?',
        sub_name: 'Ticking or rattling noise'
    },
    {
        id: 52,
        name: 'Unable to figure out?',
        sub_name: 'Squeaking or Squealing noise'
    },
    {
        id: 53,
        name: 'Unable to figure out?',
        sub_name: 'AC not cooling enough'
    },
    {
        id: 54,
        name: 'Unable to figure out?',
        sub_name: 'Feels like steering hard'
    },
    {
        id: 55,
        name: 'Unable to figure out?',
        sub_name: 'Feels problem in brake'
    },
    {
        id: 56,
        name: 'Unable to figure out?',
        sub_name: 'Shocker and Suspension'
    },
    {
        id: 57,
        name: 'Unable to figure out?',
        sub_name: 'Clutch hard'
    },
    {
        id: 58,
        name: 'Unable to figure out?',
        sub_name: 'Feels problem in Shifting gears'
    },
    {
        id: 59,
        name: 'Lights and Horns',
        sub_name: 'Horn is not working'
    },
    {
        id: 60,
        name: 'Lights and Horns',
        sub_name: 'Rear RH light bulb fuse'
    },
    {
        id: 61,
        name: 'Lights and Horns',
        sub_name: 'Rear LH light bulb fuse'
    },
    {
        id: 62,
        name: 'Lights and Horns',
        sub_name: 'Front RH light bulb fuse'
    },
    {
        id: 63,
        name: 'Lights and Horns',
        sub_name: 'Front LH light bulb fuse'
    },
    {
        id: 64,
        name: 'Lights and Horns',
        sub_name: 'LH headlight replacement'
    },
    {
        id: 65,
        name: 'Lights and Horns',
        sub_name: 'RH headlight replacement'
    },
    {
        id: 66,
        name: 'Lights and Horns',
        sub_name: 'LH tail light replacement'
    },
    {
        id: 67,
        name: 'Lights and Horns',
        sub_name: 'RH tail light replacement'
    },
    {
        id: 68,
        name: 'Lights and Horns',
        sub_name: 'Indicator bulb replacement'
    },
    {
        id: 69,
        name: 'Lights and Horns',
        sub_name: 'Brake light replacement'
    },
    {
        id: 70,
        name: 'Lights and Horns',
        sub_name: 'Any other we missed?'
    },
    {
        id: 71,
        name: 'Lost key?',
        sub_name: 'Unable to open door'
    },
    {
        id: 72,
        name: 'Others'
    }
];
