import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';

import { history } from '../services/history';

import {
    HomePage,
    About,
    Services,
    GarageSearch,
    GarageProfile,
    BootStrap,
    Admin
} from '../components/pages';
import PreviousQuote from '../components/pages/user-profile/PreviousQuote';
import UserProfile from '../components/pages/user-profile/UserProfile';

function LoadRouter() {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/home">
                    <BootStrap />
                </Route>
                <Route exact path="/">
                    <HomePage />
                </Route>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/services">
                    <Services />
                </Route>
                <Route path="/garages" exact>
                    <GarageSearch />
                </Route>
                <Route path="/garages/profile">
                    <GarageProfile />
                </Route>
                <Route path="/user/profile" exact>
                    <UserProfile />
                </Route>
                <Route path="/user/profile/request-history">
                    <PreviousQuote />
                </Route>
                <Route path="/VLX269LHG7/admin">
                    <Admin />
                </Route>
            </Switch>
        </Router>
    );
}

export default LoadRouter;
