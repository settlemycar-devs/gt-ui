import History from './history';

export default {
    goBack() {
        History.goBack();
    },
    gotoHome() {
        History.push({
            pathname: '/'
        });
    },
    gotoProfile(options = { isOngoing: false }) {
        History.push({
            pathname: `/user/profile`,
            search: `?${options.isOngoing ? 'quote=true' : ''}`
        });
    },
    gotoPreviousQuoteRequest(options = { isOngoing: true }) {
        History.push({
            pathname: `/user/profile/request-history${
                options.isOngoing ? '' : '/#previous-history'
            }`
        });
    },
    gotoGarageList() {
        History.push({
            pathname: '/garages'
        });
    },

    gotoGarageProfile(garageId) {
        History.push({
            pathname: `/garages/profile`,
            search: `?id=${garageId}`
        });
    },

    goHomeWithRefresh(options = {}) {
        let search = '';
        if (options.showLogin) {
            search = `?showLogin=${options.showLogin}`;
        }
        History.replace({
            pathname: '/home',
            search
        });
    }
};
