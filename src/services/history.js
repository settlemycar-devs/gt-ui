import { createBrowserHistory } from 'history';
import queryString from 'query-string';

export const history = createBrowserHistory();

export default {
    /**
     * options = {pathname: '', search: '?key=value', state={some: 'state'} }
     */
    push(options) {
        if (typeof options !== 'object') {
            throw new Error('Invalid argument to push method');
        }
        history.push(options);
    },
    goBack() {
        history.goBack();
    },
    replace(options) {
        history.replace(options);
    },
    parseQueryParams() {
        const search = history.location.search;
        const parsed = queryString.parse(search);
        return parsed;
    }
};
