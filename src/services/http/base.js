import queryString from 'query-string';
import fetch from 'isomorphic-unfetch';
import config from '../../config';
import StorageService from '../storage';
import navigation from '../navigation';

const BASE_URL = config.API_HOST;
class BaseHttp {
    /**
    {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    }
     */
    async performFetch(path, options = { method: 'GET' }) {
        try {
            if (options.method === 'POST') {
                const headers = options.headers || {};
                headers['Content-Type'] = 'application/json';
                options.headers = headers;
            }
            // add token to the headers
            const headers = options.headers || {};
            const token = StorageService.getToken();
            if (token) {
                headers['Authorization'] = `token ${token}`;
                options.headers = headers;
            }

            console.log('fetching:::: ', path);
            console.log('payload:', options);
            const response = await fetch(path, options);
            if (response.status >= 400 && response.status < 600) {
                if (response.status === 400) {
                    const data = await response.json();
                    throw new Error(data.message || 'Invalid Request');
                }
                if (response.status === 401) {
                    navigation.goHomeWithRefresh({ showLogin: true });
                }
                throw new Error('Bad Response from server');
            }
            const result = await response.json();
            console.log('Response::', result);
            return result;
        } catch (error) {
            throw error;
        }
    }

    /**
     * get full path with base url + url string
     */
    getFullPath(path, query = {}) {
        const baseUrl = query.base_url || BASE_URL;
        delete query.base_url;
        let url = `${baseUrl}${path}`;
        if (query && Object.keys(query).length > 0) {
            url = `${url}?${queryString.stringify(query)}`;
        }
        return url;
    }
}

export default BaseHttp;
