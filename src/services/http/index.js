import BaseHTTP from './base';

class APIService extends BaseHTTP {
    async init() {
        try {
            const url = this.getFullPath(`/app/init`);
            return await this.performFetch(url);
        } catch (error) {
            throw error;
        }
    }

    async getGarageProfile(profileId) {
        const url = this.getFullPath(`/garages/${profileId}`);
        return await this.performFetch(url);
    }

    async getGaragesList(query) {
        try {
            query = query || {};
            const url = this.getFullPath(`/garages`, query);
            return await this.performFetch(url);
        } catch (error) {
            throw error;
        }
    }

    async getPlaces(searchTerm, options = {}) {
        const query = `${searchTerm}`;
        const latitude = options.latitude;
        const longitude = options.longitude;
        const qs = {
            query,
            latitude,
            longitude
        };
        try {
            const url = this.getFullPath(`/places/autocomplete`, qs);
            return await this.performFetch(url);
        } catch (error) {
            throw error;
        }
    }

    async getPlaceDetail(placeId, options = {}) {
        const qs = {
            place_id: placeId
        };
        try {
            const url = this.getFullPath(`/places/details`, qs);
            return await this.performFetch(url);
        } catch (error) {
            throw error;
        }
    }

    async getGarageReviews(garageId, options = {}) {
        const qs = {
            garage_id: garageId
        };
        try {
            const url = this.getFullPath('/reviews', qs);
            return await this.performFetch(url);
        } catch (error) {
            throw error;
        }
    }

    async sendOtp(mobile) {
        const payload = {
            method: 'POST',
            body: JSON.stringify({
                phone: mobile
            })
        };
        try {
            const url = this.getFullPath('/auth/send-otp');
            return await this.performFetch(url, payload);
        } catch (error) {
            throw error;
        }
    }

    async verifyOTP(userId, otp) {
        const payload = {
            method: 'POST',
            body: JSON.stringify({
                user_id: userId,
                otp
            })
        };
        try {
            const url = this.getFullPath('/auth/verify-otp');
            return await this.performFetch(url, payload);
        } catch (error) {
            throw error;
        }
    }

    async fetchProfile() {
        try {
            const url = this.getFullPath('/user/profile');
            return await this.performFetch(url, {
                method: 'GET',
                isAuthenticated: true
            });
        } catch (error) {
            throw error;
        }
    }

    async reviewGarage(options = {}) {
        try {
            const url = this.getFullPath('/quotation/review-request');
            // return { data: { check_email: true } };
            return await this.performFetch(url, {
                method: 'GET',
                isAuthenticated: true
            });
        } catch (error) {
            throw error;
        }
    }

    async requestQuote(options = {}) {
        try {
            const url = this.getFullPath('/quotation/create-request');
            return await this.performFetch(url, {
                method: 'POST',
                isAuthenticated: true,
                body: JSON.stringify(options)
            });
        } catch (error) {
            throw error;
        }
    }

    async getAllQuoteRequest(qs = {}, options = {}) {
        try {
            const url = this.getFullPath('/quotation/all-quotes', qs);
            return await this.performFetch(url, {
                method: 'GET',
                isAuthenticated: true
            });
        } catch (error) {
            throw error;
        }
    }

    async fetchQuoteRequests(options = {}) {
        try {
            const url = this.getFullPath('/quotation/requested-quotes');
            return await this.performFetch(url, {
                method: 'GET',
                isAuthenticated: true
            });
        } catch (error) {
            throw error;
        }
    }

    async updateProfile(options = {}) {
        try {
            const url = this.getFullPath('/user/profile');
            return await this.performFetch(url, {
                method: 'POST',
                isAuthenticated: true,
                body: JSON.stringify(options)
            });
        } catch (error) {
            throw error;
        }
    }

    async acceptQuoteRequest(requestId, options = {}) {
        try {
            const url = this.getFullPath(
                `/quotation/${requestId}/accept-request`
            );
            return await this.performFetch(url, {
                method: 'POST',
                isAuthenticated: true,
                body: JSON.stringify(options)
            });
        } catch (error) {
            throw error;
        }
    }

    async updateQuoteRequest(requestId, options = {}) {
        try {
            const url = this.getFullPath(
                `/quotation/${requestId}/update-request`
            );
            return await this.performFetch(url, {
                method: 'POST',
                isAuthenticated: true,
                body: JSON.stringify(options)
            });
        } catch (error) {
            throw error;
        }
    }
}

export default new APIService();
