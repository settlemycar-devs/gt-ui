export default {
    getItem(key, defaultValue) {
        const value = localStorage.getItem(key);
        if (!value) {
            return defaultValue;
        }
        try {
            return JSON.parse(value);
        } catch {
            return value;
        }
    },

    setItem(key, value) {
        localStorage.setItem(key, value);
    },

    setToken(token) {
        this.setItem('token', token);
    },

    getToken() {
        return this.getItem('token', '');
    },

    setUserDetails(userData = {}) {
        this.setItem('user', JSON.stringify(userData));
    },

    getUserDetails() {
        return this.getItem('user', {});
    }
};
