import React from 'react';
import Proptypes from 'prop-types';

import { GarageSearchProvider } from './GarageSearchContext';
import { UserContextProvider } from './UserContext';

export default function AppContext({ children }) {
    return (
        <GarageSearchProvider>
            <UserContextProvider>{children}</UserContextProvider>
        </GarageSearchProvider>
    );
}

AppContext.propTypes = {
    children: Proptypes.node.isRequired
};
