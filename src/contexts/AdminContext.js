import React, { createContext, useState, useEffect, useContext } from 'react';
import _get from 'lodash/get';
import _assign from 'lodash/assign';
import PropTypes from 'prop-types';
import _isEmpty from 'lodash/isEmpty';
import APIService from '../services/http';

export const AdminContext = createContext();

export function AdminContextProvider({ children }) {
    const [adminContext, setAdminContext] = useState({
        totalData: [],
        data: [],
        meta: {},
        isLoadingRequestData: false,
        currentFilterForRequests: {}
    });

    const fetchAllQuotes = async (resetPreviousRecord = false, query = {}) => {
        try {
            if (resetPreviousRecord) {
                setAdminContext({
                    ...adminContext,
                    isLoadingRequestData: true,
                    totalData: [],
                    data: [],
                    meta: {}
                });
            } else {
                setAdminContext({ ...adminContext, isLoadingGarageData: true });
            }
            const quotations = await APIService.getAllQuoteRequest(query);
            setAdminContext(currentAdminContext => {
                const totalData = [
                    ...currentAdminContext.totalData,
                    ...quotations.data
                ];
                return {
                    ...currentAdminContext,
                    data: quotations.data,
                    meta: quotations.meta,
                    totalData,
                    isLoadingGarageData: false
                };
            });
        } catch (error) {
            setAdminContext({ ...adminContext, isLoadingGarageData: false });
            console.log('error:', error);
        }
    };

    const updateCurrentFilter = data => {
        const newFilter = {};
        if (_get(data, 'status.value')) {
            _assign(newFilter, { status: _get(data, 'status.value') });
        }
        setAdminContext({
            ...adminContext,
            currentFilterForRequests: {
                ...adminContext.currentFilterForRequests,
                ...newFilter
            }
        });
    };

    const handleLoadMore = async () => {
        const metaInfo = adminContext.meta || {};
        if (!metaInfo.offset) {
            return;
        }
        try {
            const payload = {
                offset: metaInfo.offset
            };
            _assign(payload, adminContext.currentFilterForRequests);
            if (metaInfo.limit) {
                payload.limit = metaInfo.limit;
            }
            const quotations = await APIService.getAllQuoteRequest(payload);
            setAdminContext(currentAdminContext => {
                const totalData = [
                    ...currentAdminContext.totalData,
                    ...quotations.data
                ];
                return {
                    data: quotations.data,
                    meta: quotations.meta,
                    totalData
                };
            });
        } catch (error) {
            console.log('error fetching garages:', error);
        }
    };

    useEffect(() => {
        if (_isEmpty(adminContext.currentFilterForRequests)) {
            return;
        }
        fetchAllQuotes(
            true,
            _assign({}, adminContext.currentFilterForRequests)
        );
    }, [adminContext.currentFilterForRequests]);

    return (
        <AdminContext.Provider
            value={{
                adminContext,
                fetchAllQuotes,
                handleLoadMore,
                updateCurrentFilter
            }}
        >
            {children}
        </AdminContext.Provider>
    );
}

AdminContextProvider.propTypes = {
    children: PropTypes.node.isRequired
};
