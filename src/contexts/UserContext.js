import React, { createContext, useState, useEffect } from 'react';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import _isEmpty from 'lodash/isEmpty';
import APIService from '../services/http';
import StorageService from '../services/storage';
import Navigation from '../services/navigation';

export const UserContext = createContext();

export function UserContextProvider({ children }) {
    const user = StorageService.getUserDetails();
    const token = StorageService.getToken();
    const [userContext, setUserContext] = useState({
        user,
        token,
        showLoginModal: false,
        isOtpSent: false,
        mobile: '',
        otp: '',
        error: '',
        userId: '',
        isVerifyingOTP: false,
        isSendingOTP: false,
        isProfileLoading: false,
        isLoginSuccess: false,
        showProfileDropdown: false,
        isQuoteRequestLoading: false,
        quoteRequests: []
    });

    const setShowLoginModal = showLoginModal => {
        setUserContext(currentUserContext => ({
            ...currentUserContext,
            showLoginModal
        }));
    };

    const closeLoginModal = () => {
        setUserContext(currentUserContext => ({
            ...currentUserContext,
            showLoginModal: false
        }));
    };

    const setMobile = mobile => {
        if (mobile.length > 10) {
            return;
        }
        setUserContext({ ...userContext, mobile, error: '' });
    };

    const setOTP = otp => {
        if (otp.length > 4) {
            return;
        }
        setUserContext({ ...userContext, otp, error: '' });
    };

    const setError = error => {
        setUserContext({ ...userContext, error });
    };

    const setIsVerifyingOTP = isVerifyingOTP => {
        setUserContext({ ...userContext, isVerifyingOTP });
    };

    const setIsSendingOTP = isSendingOTP => {
        setUserContext({ ...userContext, isSendingOTP, isLoginSuccess: false });
    };

    const setIsProfileLoading = isProfileLoading => {
        setUserContext(currentUserContext => ({
            ...currentUserContext,
            isProfileLoading
        }));
    };

    const fetchAndSetUserDetails = async () => {
        setIsProfileLoading(true);
        try {
            const data = await APIService.fetchProfile();
            const profileData = _get(data, 'data');
            setUserContext({ ...userContext, user: profileData });
            StorageService.setUserDetails(profileData);
        } catch (error) {
            console.log('error fetching profile', error);
        }
    };

    const handleLoginSubmit = async e => {
        e.preventDefault();
        if (userContext.isSendingOTP || userContext.isVerifyingOTP) {
            return;
        }
        if (!userContext.isOtpSent) {
            // send otp
            const { mobile } = userContext;
            if (_isEmpty(mobile) || mobile.length !== 10) {
                setError('Please Enter valid mobile number');
                return;
            }
            try {
                setIsSendingOTP(true);
                const response = await APIService.sendOtp(mobile);
                setIsSendingOTP(false);
                setUserContext({
                    ...userContext,
                    userId: _get(response, 'data._id'),
                    isOtpSent: true
                });
            } catch (error) {
                setIsSendingOTP(false);
                setError(error.message);
            }
        } else {
            // verify otp
            const { userId, otp } = userContext;
            try {
                setIsVerifyingOTP(true);
                const responseData = await APIService.verifyOTP(userId, otp);
                StorageService.setToken(_get(responseData, 'data.token'));
                setUserContext({
                    ...userContext,
                    showLoginModal: false,
                    otp: '',
                    isVerifyingOTP: false,
                    isLoginSuccess: true,
                    token: _get(responseData, 'data.token')
                });
            } catch (error) {
                setIsVerifyingOTP(false);
                setError(error.message);
            }
        }
    };

    const logout = (doNotGoHome = false) => {
        setUserContext({ ...userContext, user: {}, token: '' });
        StorageService.setToken('');
        StorageService.setUserDetails({});
        if (doNotGoHome) {
            return;
        }
        Navigation.goHomeWithRefresh();
    };

    const setShowProfileDropdown = showProfileDropdown => {
        if (!showProfileDropdown) {
            document.removeEventListener('click', documentClickHandler);
        }
        setUserContext(currentUserContext => {
            return {
                ...currentUserContext,
                showProfileDropdown
            };
        });
    };

    const documentClickHandler = () => {
        setShowProfileDropdown(false);
    };

    const handleProfileClick = () => {
        if (!userContext.showProfileDropdown) {
            document.addEventListener('click', documentClickHandler);
        }
        setShowProfileDropdown(!userContext.showProfileDropdown);
    };

    const isLoggedIn = () => {
        if (_isEmpty(userContext.user) || _isEmpty(userContext.token)) {
            return false;
        }
        return true;
    };

    const setIsQuoteRequestLoading = isQuoteRequestLoading => {
        setUserContext({ ...userContext, isQuoteRequestLoading });
    };

    const fetchAndSetQuoteRequest = async () => {
        setIsQuoteRequestLoading(true);
        try {
            const responseData = await APIService.fetchQuoteRequests();
            setUserContext({
                ...userContext,
                isQuoteRequestLoading: false,
                quoteRequests: responseData.data
            });
        } catch (error) {
            setIsQuoteRequestLoading(false);
        }
    };

    const setUserDetail = data => {
        StorageService.setUserDetails(data);
        setUserContext({ ...userContext, user: data });
    };

    useEffect(() => {
        if (userContext.isLoginSuccess) {
            fetchAndSetUserDetails();
        }
    }, [userContext.isLoginSuccess]);

    return (
        <UserContext.Provider
            value={{
                userContext,
                setShowLoginModal,
                closeLoginModal,
                handleLoginSubmit,
                setMobile,
                setOTP,
                logout,
                handleProfileClick,
                isLoggedIn,
                fetchAndSetQuoteRequest,
                setUserDetail
            }}
        >
            {children}
        </UserContext.Provider>
    );
    
}

UserContextProvider.propTypes = {
    children: PropTypes.node.isRequired
};
