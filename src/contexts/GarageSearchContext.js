import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';

import APIService from '../services/http';
import _isEmpty from 'lodash/isEmpty';

export const GarageSearchContext = React.createContext();

const radioList = [
    {
        label: 'Petrol',
        name: 'version',
        value: 'Petrol'
    },
    {
        label: 'CNG',
        name: 'version',
        value: 'CNG'
    },
    {
        label: 'Hybrid',
        name: 'version',
        value: 'Hybrid'
    }
];

const generateYears = () => {
    const years = [];
    for (let i = 2020; i > 1970; i -= 1) {
        years.push({ label: i, value: i });
    }
    return years;
};

export function GarageSearchProvider({ children }) {
    const [searchContext, setSearchContext] = useState({
        brandName: '',
        model: '',
        year: '',
        location: {},
        otherDetails: '',
        version: '',
        placeInfo: {},
        placeId: '',
        selectedService: {},
        brands: [],
        brandModels: [],
        makeYears: generateYears(),
        versionList: radioList,
        previousPlaceId: '',
        showQuoteReviewModal: false,
        email: '',
        requestForGarageId: '',
        isRequestingQuote: false,
        requestQuoteSuccesForGarageId: '',
        quoteRequestSuccessGarages: [],
        requestQuoteError: ''
    });
    const [garageData, setGarageData] = useState({
        totalData: [],
        data: [],
        meta: {},
        isLoadingGarageData: false
    });

    const setEmail = email => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            email
        }));
    };

    const setBrandName = brandName => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            brandName
        }));
    };

    const setSelectedService = selectedService => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            selectedService
        }));
    };

    const setModel = model => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            model
        }));
    };

    const setYear = year => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            year
        }));
    };

    const setOtherDetails = otherDetails => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            otherDetails
        }));
    };

    const setVersion = version => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            version
        }));
    };

    const setLocation = location => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            location
        }));
    };

    useEffect(() => {
        if (searchContext.placeId) {
            // fetch lat long and set location
            APIService.getPlaceDetail(searchContext.placeId)
                .then(placeDetailResponse => {
                    setLocation({
                        latitude: _get(placeDetailResponse, 'data.latitude'),
                        longitude: _get(placeDetailResponse, 'data.longitude')
                    });
                })
                .catch(err => console.log('error', err));
        }
    }, [searchContext.placeId]);

    const updateSearchContext = (values = {}) => {
        setSearchContext({ ...searchContext, ...values });
    };

    const updateGarageData = values => {
        setGarageData({ ...garageData, ...values });
    };

    const fetchAndSetGarages = async (resetPreviousRecord = false) => {
        try {
            const payload = {
                latitude: _get(searchContext, 'location.latitude'),
                longitude: _get(searchContext, 'location.longitude')
            };
            if (resetPreviousRecord) {
                setGarageData({
                    ...garageData,
                    isLoadingGarageData: true,
                    totalData: [],
                    data: [],
                    meta: {}
                });
            } else {
                setGarageData({ ...garageData, isLoadingGarageData: true });
            }
            const garages = await APIService.getGaragesList(payload);
            setGarageData(currentGarageData => {
                const totalData = [
                    ...currentGarageData.totalData,
                    ...garages.data
                ];
                return {
                    ...currentGarageData,
                    data: garages.data,
                    meta: garages.meta,
                    totalData,
                    isLoadingGarageData: false
                };
            });
        } catch (error) {
            setGarageData({ ...garageData, isLoadingGarageData: false });
            console.log('error:', error);
        }
    };

    const setPlaceInfo = placeInfo => {
        setSearchContext(currentSearchContext => ({
            ...currentSearchContext,
            placeInfo,
            placeId: placeInfo.value === 'My Location' ? '' : placeInfo.value,
            previousPlaceId: currentSearchContext.placeId
        }));
    };

    const handleLoadMoreGarages = async () => {
        const metaInfo = garageData.meta || {};
        if (!metaInfo.offset) {
            return;
        }
        try {
            const payload = {
                offset: metaInfo.offset,
                latitude: _get(searchContext, 'location.latitude'),
                longitude: _get(searchContext, 'location.longitude')
            };
            if (metaInfo.limit) {
                payload.limit = metaInfo.limit;
            }
            const garages = await APIService.getGaragesList(payload);
            setGarageData(currentGarageData => {
                const totalData = [
                    ...currentGarageData.totalData,
                    ...garages.data
                ];
                return {
                    ...currentGarageData,
                    data: garages.data,
                    meta: garages.meta,
                    totalData
                };
            });
        } catch (error) {
            console.log('error fetching garages:', error);
        }
    };

    const setShowQuoteReviewModal = showQuoteReviewModal => {
        setSearchContext({ ...searchContext, showQuoteReviewModal });
    };

    const requestQuote = async () => {
        setSearchContext({ ...searchContext, isRequestingQuote: true });
        const selectedService = searchContext.selectedService;
        const serviceName = `${selectedService.name} ${
            selectedService.sub_name ? `(${selectedService.sub_name})` : ''
        }`;
        try {
            const payload = {
                garage_id: searchContext.requestForGarageId,
                brand_name: _get(searchContext, 'brandName.brand'),
                model: _get(searchContext, 'model.value'),
                year: _get(searchContext, 'year.value'),
                other_details: searchContext.otherDetails,
                email: _get(searchContext, 'email'),
                service_name: serviceName,
                location: [
                    _get(searchContext, 'location.longitude'),
                    _get(searchContext, 'location.latitude')
                ], // long, lat
                address: _get(searchContext, 'placeInfo.description')
            };
            const response = await APIService.requestQuote(payload);
            if (_get(response, 'data.success')) {
                setSearchContext({
                    ...searchContext,
                    isRequestingQuote: false,
                    showQuoteReviewModal: false,
                    requestQuoteSuccesForGarageId:
                        searchContext.requestForGarageId,
                    quoteRequestSuccessGarages: [
                        ...searchContext.quoteRequestSuccessGarages,
                        searchContext.requestForGarageId
                    ]
                });
            }
        } catch (error) {
            setSearchContext({
                ...searchContext,
                isRequestingQuote: false,
                requestQuoteError:
                    error.message ||
                    'Something went Wrong while request quote! Try Again'
            });
        }
    };

    const openGarageRequestModal = garageId => {
        setSearchContext({
            ...searchContext,
            showQuoteReviewModal: true,
            requestForGarageId: garageId
        });
    };

    useEffect(() => {
        async function init() {
            console.log('calling init');
            if (_isEmpty(searchContext.brands)) {
                try {
                    const response = await APIService.init();
                    setSearchContext({
                        ...searchContext,
                        brands: response.data
                    });
                } catch (error) {
                    console.log('error', error);
                }
            }
        }
        init();
    }, []);

    return (
        <GarageSearchContext.Provider
            value={{
                searchContext,
                garageData,
                updateGarageData,
                updateSearchContext,
                fetchAndSetGarages,
                setPlaceInfo,
                setBrandName,
                setModel,
                setYear,
                setEmail,
                setOtherDetails,
                setSelectedService,
                setVersion,
                handleLoadMoreGarages,
                setShowQuoteReviewModal,
                requestQuote,
                openGarageRequestModal,
                setLocation
            }}
        >
            {children}
        </GarageSearchContext.Provider>
    );
}

GarageSearchProvider.propTypes = {
    children: PropTypes.node.isRequired
};
