import React from 'react';
import GTLogoFooter from '../../asset/images/gt-logo-footer.png';

import './Footer.css';

export default function Footer() {
    return (
        <div className="footer-section">
            <div className="footer-wrapper">
                <div className="logo-section">
                    <img src={GTLogoFooter} />
                </div>
                <div className="copyright-section">
                    <div>Copyright © 2019 garagetroop .</div>
                    <div>All rights reserved.</div>
                </div>
            </div>
        </div>
    );
}
