import React from 'react';
import { TopHeader, SubHeader } from '.';

export default function CompleteHeader() {
    return (
        <div>
            <TopHeader />
            <SubHeader />
        </div>
    );
}
