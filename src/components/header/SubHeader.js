import React, { Component } from 'react';

import './SubHeader.css';

import { SearchForm } from '../search';

export default class SubHeader extends Component {
    render() {
        return (
            <div className="subheader-img-container">
                <div className="subheader-img-inner-container">
                    <h1 className="header-web">
                        India’s <br /> First Garage <br /> Discovery Platform
                    </h1>
                    <h1 className="header-mobile">
                        India’s First Garage <br /> Discovery Platform
                    </h1>
                    {/* <h1>Largest Garage</h1>
                    <h1>Discovery Platform</h1> */}
                </div>
                <div className="subheader-form-container">
                    <SearchForm />
                </div>
            </div>
        );
    }
}
