import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import './ProfileDropDownMenu.css';
import Navigation from '../../services/navigation';
import { UserContext } from '../../contexts/UserContext';

export default function ProfileDropDownMenu({ showMenu }) {
    const { logout } = useContext(UserContext);
    if (!showMenu) {
        return null;
    }

    return (
        <div className="profile-dd-menu">
            <div className="profile-dd-menu-inner">
                <div
                    className="profile-dd-item"
                    onClick={() => {
                        setTimeout(() => {
                            Navigation.gotoProfile();
                        }, 100);
                    }}
                >
                    Profile Setting
                </div>
                <div
                    className="profile-dd-item"
                    onClick={() => {
                        setTimeout(() => {
                            Navigation.gotoProfile({
                                isOngoing: true
                            });
                        }, 100);
                    }}
                >
                    Quote History
                </div>
                <div
                    className="profile-dd-item"
                    onClick={() => {
                        logout();
                    }}
                >
                    Logout
                </div>
            </div>
        </div>
    );
}

ProfileDropDownMenu.propTypes = {
    showMenu: PropTypes.bool.isRequired
};

ProfileDropDownMenu.defaultProps = {
    showMenu: false
};
