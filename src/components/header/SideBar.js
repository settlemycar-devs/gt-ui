import React, { useContext } from 'react';
import { slide as Menu } from 'react-burger-menu';
import BarIcon from '../../asset/icons/bars.svg';
import { Link } from 'react-router-dom';
import Navigation from '../../services/navigation';

import './bm-style.css';
import { UserContext } from '../../contexts/UserContext';

export default function SideBar() {
    const { isLoggedIn, logout } = useContext(UserContext);
    return (
        <Menu customBurgerIcon={<img src={BarIcon} />}>
            {window.location.pathname !== '/' && (
                <div className="gt-nav-item">
                    <Link to="/">Home</Link>
                </div>
            )}
            {isLoggedIn() && (
                <>
                    <div
                        className="bm-item gt-nav-item"
                        onClick={() => {
                            setTimeout(() => {
                                Navigation.gotoProfile();
                            }, 100);
                        }}
                    >
                        Profile Setting
                    </div>
                    <div
                        className="bm-item gt-nav-item"
                        onClick={() => {
                            setTimeout(() => {
                                Navigation.gotoProfile({
                                    isOngoing: true
                                });
                            }, 100);
                        }}
                    >
                        Quote History
                    </div>
                </>
            )}
            <div className="gt-nav-item">
                <Link to="/services">Services</Link>
            </div>
            <div className="gt-nav-item">
                <Link to="/about">About Us</Link>
            </div>
            {isLoggedIn() && (
                <div
                    className="bm-item gt-nav-item "
                    onClick={() => {
                        logout();
                    }}
                >
                    Logout
                </div>
            )}
        </Menu>
    );
}
