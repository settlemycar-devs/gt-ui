import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserIcon from '../../asset/icons/user-circle.svg';
import './TopHeader.css';
import gtLogo from '../../asset/images/Garagetroop(2).png';
import { UserContext } from '../../contexts/UserContext';
import ProfileDropDownMenu from './ProfileDropDownMenu';
import SideBar from './SideBar';

export default function TopHeader() {
    const {
        userContext: { showProfileDropdown },
        handleProfileClick,
        isLoggedIn,
        setShowLoginModal
    } = useContext(UserContext);
    return (
        <div className="topheader-container">
            <SideBar />
            <div className="container-fluid">
                <div className="row header-row">
                    <ProfileDropDownMenu showMenu={showProfileDropdown} />
                    <div className="col-md-6 col-sm-12 gt-logo-wrapper">
                        <div>
                            <Link to="/">
                                <img src={gtLogo} alt="garagetroop" />
                            </Link>
                        </div>
                        {!isLoggedIn() && (
                            <div className="gt-login-btn-mobile">
                                <button
                                    className="btn btn-danger btn-sm"
                                    onClick={() => {
                                        setShowLoginModal(true);
                                    }}
                                >
                                    Login
                                </button>
                            </div>
                        )}
                    </div>
                    <div className="col-md-6 d-flex justify-content-end">
                        <div className="gt-nav">
                            <div className="gt-nav-item">
                                <Link to="/services">services</Link>
                            </div>
                            <div className="gt-nav-item">
                                <Link to="/about">About Us</Link>
                            </div>
                            {!isLoggedIn() && (
                                <div className="gt-nav-item">
                                    <button
                                        className="btn btn-danger btn-sm"
                                        onClick={() => {
                                            setShowLoginModal(true);
                                        }}
                                    >
                                        Login
                                    </button>
                                </div>
                            )}
                            {isLoggedIn() && (
                                <div
                                    className="gt-nav-item profile"
                                    onClick={() => {
                                        handleProfileClick();
                                    }}
                                >
                                    <div>Profile</div>
                                    <img src={UserIcon} />
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
