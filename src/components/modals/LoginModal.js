import React, { useContext } from 'react';
import { UserContext } from '../../contexts/UserContext';
import Modal from '../shared/Modal';
import LoginForm from '../login/LoginForm';

export default function LoginModal() {
    const {
        userContext: { showLoginModal },
        setShowLoginModal
    } = useContext(UserContext);
    if (!showLoginModal) {
        return null;
    }
    return (
        <Modal
            onCloseIconAction={() => {
                setShowLoginModal(false);
            }}
        >
            <LoginForm />
        </Modal>
    );
}
