import React, { useContext } from 'react';
import Modal from '../shared/Modal';
import { GarageSearchContext } from '../../contexts/GarageSearchContext';
import QuoteReviewForm from '../quote/QuoteReviewForm';

export default function QuoteReviewModal() {
    const {
        searchContext: { showQuoteReviewModal },
        setShowQuoteReviewModal
    } = useContext(GarageSearchContext);
    if (!showQuoteReviewModal) {
        return null;
    }
    return (
        <Modal
            onCloseIconAction={() => {
                setShowQuoteReviewModal(false);
            }}
        >
            <QuoteReviewForm />
        </Modal>
    );
}
