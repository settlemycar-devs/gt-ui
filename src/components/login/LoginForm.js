import React, { useContext } from 'react';

import './LoginForm.css';
import { UserContext } from '../../contexts/UserContext';

export default function LoginModal() {
    const {
        userContext: {
            mobile,
            otp,
            isOtpSent,
            isVerifyingOTP,
            isSendingOTP,
            error
        },
        handleLoginSubmit,
        setMobile,
        setOTP
    } = useContext(UserContext);
    return (
        <div className="login-form">
            <div className="login-form-content">
                <div className="login-form-header">
                    <h4 className="login-form-title">Sign In</h4>
                </div>
                <div className="login-form-body">
                    <form onSubmit={handleLoginSubmit}>
                        <div className="form-group">
                            <div className="input-group">
                                <input
                                    type="number"
                                    className="form-control"
                                    name="mobile"
                                    placeholder="Mobile Number"
                                    value={mobile}
                                    onChange={e => setMobile(e.target.value)}
                                    required="required"
                                />
                            </div>
                        </div>
                        {isOtpSent && (
                            <div className="form-group">
                                <div className="input-group">
                                    <input
                                        type="number"
                                        className="form-control"
                                        name="otp"
                                        placeholder="Enter 4 digit OTP"
                                        value={otp}
                                        onChange={e => setOTP(e.target.value)}
                                        required="required"
                                    />
                                </div>
                            </div>
                        )}
                        <div style={{ color: 'red', marginBottom: '10px' }}>
                            {error}
                        </div>
                        <div className="form-group">
                            <button
                                type="submit"
                                className={`btn btn-primary btn-block btn-lg ${
                                    isVerifyingOTP || isSendingOTP
                                        ? 'disabled'
                                        : ''
                                }`}
                            >
                                {isOtpSent ? 'Verify OTP' : 'Send OTP'}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}
