import React from 'react';
import './App.css';

import AppContext from '../../contexts';
import LoadRouter from '../../routes';
import { LoginModal, QuoteReviewModal } from '../modals';

function App() {
    return (
        <AppContext>
            <LoadRouter />
            <LoginModal />
            <QuoteReviewModal />
        </AppContext>
    );
}

export default App;
