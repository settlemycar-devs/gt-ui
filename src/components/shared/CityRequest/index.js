import React, { useState } from 'react';
import Select from 'react-select';

import './CityRequest.css';

const cities = [
    { value: 'Banglore', label: 'Banglore' },
    { value: 'Mumbai', label: 'Mumbai' },
    { value: 'Prayagraj', label: 'Prayagraj' },
    { value: 'Patna', label: 'Patna' }
];

export default function CityRequest() {
    const [city, setCity] = useState('');
    const [isCitySelected, setIsCitySelected] = useState(true);
    const [email, setEmail] = useState('');
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const handleSubmit = event => {
        event.preventDefault();
        console.log('city', city);
        console.log('email', email);
        // TODO:Send this data to the server.
        setIsFormSubmitted(true);
    };

    const handleEmailChange = event => {
        setEmail(event.target.value);
    };

    const handleCityChange = value => {
        setCity(value);
    };

    if (isFormSubmitted) {
        return (
            <div className="city-request-success">
                Thank you, Your response submitted successfully
            </div>
        );
    }
    return (
        <div className="city-request">
            <form onSubmit={handleSubmit}>
                <div className="city-form-container">
                    <div className="city">
                        <Select
                            classNamePrefix={'gt-react-select'}
                            options={cities}
                            value={city}
                            placeholder="City"
                            onChange={handleCityChange}
                        />
                    </div>
                    <input
                        className="email"
                        type="email"
                        placeholder="Email"
                        value={email}
                        onChange={handleEmailChange}
                    />
                    <input
                        className="submit-btn"
                        type="submit"
                        value="Submit"
                    />
                </div>
            </form>
        </div>
    );
}
