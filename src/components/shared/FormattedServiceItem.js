import React from 'react';
import PropTypes from 'prop-types';

export default function FormattedServiceOptions({ name, sub_name }) {
    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div>{name}</div>
            <div
                style={{
                    paddingLeft: '0px',
                    fontSize: '16px',
                    color: '#ccc',
                    fontWeight: 'bolder'
                }}
            >
                {sub_name || ''}
            </div>
        </div>
    );
}

FormattedServiceOptions.propTypes = {
    name: PropTypes.string,
    sub_name: PropTypes.string
};
