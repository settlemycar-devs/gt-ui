import React from 'react';
import CityCSImage from '../../../asset/images/city-cs.png';

import './CityCS.css';

export default function CityCS() {
    return (
        <div className="city-cs">
            <div className="city-cs-text-section">
                <div className="city-cs-text-1">
                    Keep patience, we will be everywhere very soon.
                </div>
                <div className="city-cs-text-2">
                    Right now we are only in Gurgaon and Delhi
                </div>
            </div>
            <div className="city-cs-image-section">
                <img src={CityCSImage} />
            </div>
        </div>
    );
}
