import React from 'react';
import PropTypes from 'prop-types';

export default function FormattedOptions({ label, description }) {
    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div>{label}</div>
            <div
                style={{
                    paddingLeft: '4px',
                    fontSize: '12px',
                    color: '#ccc',
                    fontWeight: 700
                }}
            >
                {description || ''}
            </div>
        </div>
    );
}

FormattedOptions.propTypes = {
    label: PropTypes.string,
    description: PropTypes.string
};
