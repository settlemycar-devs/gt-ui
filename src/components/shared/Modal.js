import React from 'react';
import PropTypes from 'prop-types';
import _noop from 'lodash/noop';

import './Modal.css';

export default function Modal({ children, showCloseIcon, onCloseIconAction }) {
    // const [isCloseIconClicked, setIsCloseIconClicked] = useState(false);
    return (
        <div className={`gt-modal-container`}>
            <span
                className="gt-close-modal"
                onClick={() => {
                    if (showCloseIcon) {
                        onCloseIconAction(true);
                    }
                }}
            >
                &#10005;
            </span>
            <div className="gt-modal-content">{children}</div>
        </div>
    );
}

Modal.propTypes = {
    children: PropTypes.node.isRequired,
    showCloseIcon: PropTypes.bool,
    onCloseIconAction: PropTypes.func
};

Modal.defaultProps = {
    showCloseIcon: true,
    onCloseIconAction: _noop
};
