import React, { Component } from 'react';
import { Formik, Form, Field } from 'formik';
import _map from 'lodash/map';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import { withRouter } from 'react-router-dom';
import debounce from 'debounce-promise';

import AsyncSelect from 'react-select/async';

import APIService from '../../services/http';
import GTSelect from './GTSelect';
import GTRadioGroup from './GTRadioGroup';
import { GarageSearchContext } from '../../contexts/GarageSearchContext';
import FormattedOptions from '../shared/FormattedReactSelectItem';
import FormattedServiceOptions from '../shared/FormattedServiceItem';
import servicesList from '../../constants/garage-services';
import locationIcon from '../../asset/icons/location.svg';

import './SearchForm.css';

class RefineSearchForm extends Component {
    static contextType = GarageSearchContext;

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getPlacesFromSearchTerm = this.getPlacesFromSearchTerm.bind(this);
        this.handleNearMeClick = this.handleNearMeClick.bind(this);
        this.state = {
            modelOptions: [],
            locationsList: [],
            defaultLocationOptions: []
        };
    }

    handleSubmit() {
        const {
            searchContext: { placeId, previousPlaceId },
            fetchAndSetGarages
        } = this.context;
        if (placeId !== previousPlaceId) {
            fetchAndSetGarages(true);
        }
    }

    handleSelectChange(onChange, name, selectedValue) {
        const {
            searchContext: { brands }
        } = this.context;
        const brand = brands.find(brand => brand._id === selectedValue._id);
        const models = _map(brand.brand_models, model => {
            return {
                label: model,
                value: model
            };
        });
        this.setState({ modelOptions: models });
        onChange(name, selectedValue);
    }

    async getPlacesFromSearchTerm(currentValue) {
        try {
            const response = await APIService.getPlaces(currentValue);
            const places = response.data;
            const locationList = _map(places, place => {
                return {
                    label: place.title,
                    value: place.place_id,
                    description: place.description
                };
            });
            return locationList;
        } catch (error) {
            console.log(error);
            return [];
        }
    }

    loadOptions = inputValue => {
        return new Promise(resolve => {
            this.getPlacesFromSearchTerm(inputValue).then(locationList => {
                this.setState({ defaultLocationOptions: locationList });
                resolve(locationList);
            });
        });
    };

    validateSearchForm = values => {
        const errors = {};
        if (_isEmpty(values.location)) {
            errors.location = 'Please select location';
        }
        return errors;
    };

    handleNearMeClick() {
        const { setPlaceInfo, setLocation } = this.context;
        const successCB = currentPosition => {
            const latitude = _get(currentPosition, 'coords.latitude');
            const longitude = _get(currentPosition, 'coords.longitude');
            setPlaceInfo({
                label: 'My Location',
                value: 'My Location'
            });
            setLocation({ latitude, longitude });
            console.log('CurrentPosition:', latitude, longitude);
        };
        const errorCB = error => {
            if (error.code === 1) {
                alert(
                    `You have denied 'GarageTroop' access to your location. Check your browser settings and give permission to see garages around you.`
                );
            }
        };
        if (window.navigator) {
            window.navigator.geolocation.getCurrentPosition(successCB, errorCB);
        }
    }

    render() {
        const {
            setPlaceInfo,
            setBrandName,
            setModel,
            setYear,
            setOtherDetails,
            setVersion,
            setSelectedService,
            searchContext
        } = this.context;
        return (
            <div className="search-form-container">
                <Formik
                    onSubmit={this.handleSubmit}
                    initialValues={{
                        brandName: _get(searchContext, 'brandName', ''),
                        model: _get(searchContext, 'model', ''),
                        year: _get(searchContext, 'year', ''),
                        location: _get(searchContext, 'placeInfo'),
                        otherDetails: _get(searchContext, 'otherDetails', ''),
                        version: _get(searchContext, 'version', ''),
                        selectedService: _isEmpty(
                            _get(searchContext, 'selectedService')
                        )
                            ? ''
                            : _get(searchContext, 'selectedService')
                    }}
                    validate={this.validateSearchForm}
                    validateOnChange={false}
                    validateOnBlur={false}
                >
                    {({
                        values,
                        setFieldValue,
                        setFieldTouched,
                        errors,
                        touched,
                        setErrors
                    }) => (
                        <Form>
                            <div className="form-item">
                                <GTSelect
                                    name="brandName"
                                    value={values.brandName}
                                    onChange={(key, value) => {
                                        setFieldValue(key, value);
                                        setBrandName(value);
                                    }}
                                    handleOnChange={this.handleSelectChange}
                                    onBlur={setFieldTouched}
                                    error={errors.brandName}
                                    touched={touched.brandName}
                                    options={searchContext.brands}
                                    getOptionLabel={({ brand }) => brand}
                                    getOptionValue={({ _id }) => _id}
                                    placeholder={'Car Brand'}
                                    classNamePrefix={'gt-react-select'}
                                    defaultValue={_get(
                                        searchContext,
                                        'brandName',
                                        ''
                                    )}
                                />
                            </div>
                            <div className="model-year-container">
                                <div className="form-item">
                                    <GTSelect
                                        name={'model'}
                                        value={values.model}
                                        onChange={(key, value) => {
                                            setFieldValue(key, value);
                                            setModel(value);
                                        }}
                                        onBlur={setFieldTouched}
                                        error={errors.model}
                                        touched={touched.model}
                                        options={this.state.modelOptions}
                                        placeholder={'Model'}
                                        classNamePrefix={'gt-react-select'}
                                    />
                                </div>
                                <div className="form-item">
                                    <GTSelect
                                        name={'year'}
                                        value={values.year}
                                        onChange={(key, value) => {
                                            setFieldValue(key, value);
                                            setYear(value);
                                        }}
                                        onBlur={setFieldTouched}
                                        error={errors.year}
                                        touched={touched.year}
                                        options={searchContext.makeYears}
                                        placeholder={'Year'}
                                        classNamePrefix={'gt-react-select'}
                                    />
                                </div>
                            </div>
                            <div className="form-item">
                                <GTSelect
                                    name="selectedService"
                                    value={values.selectedService}
                                    onChange={(key, value) => {
                                        setErrors({});
                                        setFieldValue(key, value);
                                        setSelectedService(value);
                                    }}
                                    onBlur={setFieldTouched}
                                    error={errors.selectedService}
                                    touched={touched.selectedService}
                                    options={servicesList}
                                    formatOptionLabel={FormattedServiceOptions}
                                    placeholder={'Service'}
                                    classNamePrefix={'gt-react-select'}
                                />
                            </div>
                            <label className="otherdetails-label">
                                Please describe your problem in detail
                            </label>
                            <div className="form-item">
                                <Field
                                    name="otherDetails"
                                    component="textarea"
                                    placeholder="Other Details"
                                    onChange={e => {
                                        setFieldValue(
                                            'otherDetails',
                                            e.target.value
                                        );
                                        setOtherDetails(e.target.value);
                                    }}
                                />
                            </div>
                            <div className="form-item form-item-version">
                                <label>Version</label>
                                <GTRadioGroup
                                    radioList={searchContext.versionList}
                                    handleOnChange={value => {
                                        setVersion(value);
                                    }}
                                />
                            </div>
                            <div className="form-item">
                                <AsyncSelect
                                    name={'location'}
                                    loadOptions={debounce(
                                        this.loadOptions,
                                        1000
                                    )}
                                    cacheOptions
                                    onChange={value => {
                                        setErrors({});
                                        setFieldValue('location', value);
                                        setPlaceInfo(value);
                                    }}
                                    placeholder={'Location'}
                                    classNamePrefix={'gt-react-select'}
                                    formatOptionLabel={FormattedOptions}
                                    defaultInputValue={_get(
                                        searchContext,
                                        'placeInfo.description'
                                    )}
                                    defaultOptions={
                                        this.state.defaultLocationOptions
                                    }
                                    value={
                                        _isEmpty(
                                            _get(searchContext, 'placeInfo')
                                        )
                                            ? ''
                                            : _get(searchContext, 'placeInfo')
                                    }
                                />
                                <div
                                    className="near-me-btn"
                                    onClick={() => {
                                        setFieldValue('location', {
                                            label: 'My Location',
                                            value: 'My Location'
                                        });
                                        this.handleNearMeClick();
                                    }}
                                >
                                    <img src={locationIcon} alt="dummy" />
                                    <span>Near Me</span>
                                </div>
                            </div>
                            <div className="form-validation-errors">
                                {Object.keys(errors).map((error, index) => {
                                    return <li key={index}>{errors[error]}</li>;
                                })}
                            </div>
                            <div className="form-item">
                                <button
                                    type="submit"
                                    className="form-search-btn"
                                >
                                    Modify Search
                                </button>
                            </div>
                            {/* <pre>{JSON.stringify(values, null, 2)}</pre> */}
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}

RefineSearchForm.contextType = GarageSearchContext;

export default withRouter(RefineSearchForm);
