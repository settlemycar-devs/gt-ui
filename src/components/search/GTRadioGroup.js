import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import GTRadio from './GTRadio';

import './GTRadioGroup.css';
import { GarageSearchContext } from '../../contexts/GarageSearchContext';

export default function GTRadioGroup({
    radioList,
    parentClasName,
    handleOnChange
}) {
    // radioList : [{label, name, value}...]
    const {
        searchContext: { version }
    } = useContext(GarageSearchContext);
    return (
        <div className={parentClasName || 'gt-radio-group-container'}>
            {radioList &&
                radioList.map(radioItem => (
                    <GTRadio
                        key={radioItem.value}
                        {...radioItem}
                        handleOnChange={handleOnChange}
                        isChecked={radioItem.value === version}
                    />
                ))}
        </div>
    );
}

GTRadioGroup.propTypes = {
    radioList: PropTypes.array.isRequired,
    parentClasName: PropTypes.object,
    handleOnChange: PropTypes.func
};
