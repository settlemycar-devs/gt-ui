import React from 'react';
import { useField } from 'formik';
import PropTypes from 'prop-types';

import './GTRadio.css';

const GTRadio = ({ label, isChecked, handleOnChange, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <>
            <label className="gt-radio-label">
                <input
                    type="radio"
                    {...field}
                    {...props}
                    onChange={e => {
                        field.onChange(e);
                        handleOnChange(e.target.value);
                    }}
                    checked={isChecked}
                />
                {label}
            </label>
            {meta.touched && meta.error ? (
                <div className="error">{meta.error}</div>
            ) : null}
        </>
    );
};

GTRadio.propTypes = {
    label: PropTypes.string.isRequired,
    handleOnChange: PropTypes.func,
    isChecked: PropTypes.bool
};

export default GTRadio;
