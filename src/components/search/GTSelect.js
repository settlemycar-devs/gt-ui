import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

export default function GTSelect(props) {
    const {
        name,
        onChange,
        handleOnChange,
        onBlur,
        value,
        options,
        ...otherProps
    } = props;
    const handleChange = value => {
        if (handleOnChange) {
            // this is going to call setFieldValue and manually update values.topcis
            handleOnChange(onChange, name, value);
        } else {
            onChange(name, value);
        }
    };

    const handleBlur = () => {
        // this is going to call setFieldTouched and manually update touched.topcis
        onBlur(name, true);
    };

    return (
        <Select
            options={options}
            onChange={handleChange}
            onBlur={handleBlur}
            value={value}
            {...otherProps}
        />
    );
}

GTSelect.propTypes = {
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    value: PropTypes.any,
    options: PropTypes.array,
    handleOnChange: PropTypes.func
};
