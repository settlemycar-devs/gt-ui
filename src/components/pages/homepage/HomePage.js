import React, { Component } from 'react';

import ReviewSection from './ReviewSection';
import Services from './Services';
import Info from './Info';
import { CityCS, CityRequest } from '../../shared';
import { Footer } from '../../footer';
import { TopHeader, SubHeader } from '../../header';

import './HomePage.css';

const reviews = [
    {
        title:
            "First time user,great concept,would definitely use again not disappointed good price fast service,hopefully cuts out the worry of finding a good reliable garage and the rip off merchant's.Thanks again(cambelt and waterpump replacement.",
        rating: 5,
        reviewed_by: {
            name: 'Aadesh Nigam'
        },
        id: 1
    },
    {
        title:
            "We needed a clutch replacement but were unsure where to go to, and also didn't have time to be ringing round for quotes. This service gave us over 10 quotes in less than 24 hours, from which we were able to pick the best one. We would have probably paid double what we ended up paying had we gone to quick fit or similar.",
        rating: 4,
        reviewed_by: {
            name: 'Pratik Singh'
        },
        id: 2
    },
    {
        title:
            "Well done Garagetroop. Made up with Indo-german automobiles, would revisit the same garage. Job was average price and didn't take too long to fix. Will definitely be going to that garage again.",
        rating: 5,
        reviewed_by: {
            name: 'Riyaz Ahmad'
        },
        id: 3
    },
    {
        title:
            "I needed a Limited Slip Differential and clutch changing in my Toyota and I was struggling to find somewhere specialised enough to do the work. I thought I'd try this service and got a reply almost instantly. I can't recommend this setup highly enough. I would 100% use this service again and again.",
        rating: 5,
        reviewed_by: {
            name: 'Radhika Gupta'
        },
        id: 4
    },
    {
        title:
            'Have used Jindal services twice.Will definitely use it again and will recommend it to anyone who is looking for a quick response with reasonable prices and a knowledgeable mechanic in Delhi & NCR.',
        rating: 4,
        reviewed_by: {
            name: 'Nishant Sharma'
        },
        id: 5
    },
    {
        title:
            'Great convenient system from your pc, get fair quotes before having the work done. I used Auto World Gurugram,the work was completed quickly and professionally.he was the first quote to respond. i later got a few more, also had some second hand parts garages send quote,',
        rating: 5,
        reviewed_by: {
            name: 'Dinesh Bansal'
        },
        id: 6
    }
];

class HomePage extends Component {
    render() {
        return (
            <div className="home-page">
                {/* <CompleteHeader /> */}
                <TopHeader />
                <SubHeader />
                <ReviewSection reviews={reviews} isOnHomePage={true} />
                <Services />
                <Info />
                <div className="city-cs-container">
                    <CityCS />
                </div>
                <div className="city-request-section">
                    <div className="city-request-section-header">
                        Let us know from your city that you want us there
                    </div>
                    <div className="city-request-wrapper">
                        <CityRequest />
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default HomePage;
