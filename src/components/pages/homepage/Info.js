import React from 'react';
import Slider from 'react-slick';
import _map from 'lodash/map';

import InfoImage from '../../../asset/images/homepage-info-1.png';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './info.css';

const infoList = [
    {
        id: 1,
        title: 'India’s first car repair/service price comparison platform',
        sub_title: '',
        description:
            'Just choose the service you need and we will show you the best garages nearby which will give you the best price estimate to complete your service.',
        image: InfoImage
    },
    {
        id: 2,
        title: 'Virtual visit garages and get all the crisp details',
        sub_title: '',
        description:
            'You can virtually visit garages in your location by our unique profiling system, know everything about their hygiene and safety standards just by sitting at your home/offices. Forget past hassles.',
        image:
            'https://garagetroop.s3-us-west-2.amazonaws.com/assets/virtual_visit.png'
    },
    {
        id: 3,
        title: 'Enable great communication between workshops and car owners',
        sub_title: '',
        description:
            'Range of local and authorised workshops will connect you for your services and send their estimated quotes with proper justifications.',
        image:
            'https://garagetroop.s3-us-west-2.amazonaws.com/assets/enable_communication.png'
    },
    {
        id: 4,
        title: 'Now you are the boss, you can play with the estimates.',
        sub_title: '',
        description:
            'Choose the best garage that resonates with you on the basis of your convenience( Pick and Drop or Door step ), Time preferences, Hygiene practises, garage that looks good and reasonable in terms of pricing.',
        image:
            'https://garagetroop.s3-us-west-2.amazonaws.com/assets/play_with_quotes.png'
    }
];

const InfoItem = ({ info }) => {
    return (
        <div className="info-item-container">
            <div className="info-item-image-container">
                <img src={info.image} />
            </div>
            <div className="info-item-text-container">
                <div className="info-item-text-1">{info.title}</div>
                <div className="info-item-text-2">{info.sub_title}</div>
                <div className="info-item-text-3">{info.description}</div>
            </div>
        </div>
    );
};

export default function Info() {
    const settings = {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000
    };
    return (
        <div className="info-section">
            <div className="info-container">
                <Slider {...settings}>
                    {_map(infoList, info => {
                        return <InfoItem info={info} key={info.id} />;
                    })}
                </Slider>
            </div>
        </div>
    );
}
