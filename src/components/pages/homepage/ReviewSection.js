import React from 'react';
import Slider from 'react-slick';
import _get from 'lodash/get';
import _map from 'lodash/map';
import PropTypes from 'prop-types';

import './ReviewSection.css';
import LargeStar from '../../../asset/icons/star_large.svg';
import SmallStar from '../../../asset/icons/star_selected.svg';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const ReviewStar = ({ review }) => {
    const reviewList = [];
    for (let i = 0; i < review.rating; i++) {
        reviewList.push(<img src={SmallStar} key={i + 1} />);
    }
    if (reviewList.length > 0) {
        return reviewList;
    }
    return null;
};

const ReviewItem = ({ review }) => {
    return (
        <div className="review-item">
            <div className="review-item__inner">
                <div className="review-title">{_get(review, 'title')}</div>
                <div className="review-rating">
                    <div className="review-stars">
                        <ReviewStar review={review} />
                    </div>
                    <div>~ {_get(review, 'reviewed_by.name')}</div>
                </div>
            </div>
        </div>
    );
};

ReviewItem.propTypes = {
    review: PropTypes.instanceOf(Object)
};

const HomePageReviewList = ({ reviews }) => {
    const settings = {
        slidesToShow: 3,
        arrows: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                    arrows: false,
                }
            }
        ]
    };
    return (
        <div className="review-list-wrappper">
            <Slider {...settings}>
                {_map(reviews, review => {
                    return (
                        <div
                            className="review-item-wrapper"
                            key={review._id || review.id}
                        >
                            <ReviewItem review={review} />
                        </div>
                    );
                })}
            </Slider>
        </div>
    );
};

HomePageReviewList.propTypes = {
    reviews: PropTypes.array
};

const ReviewSection = ({ reviews, isOnHomePage }) => {
    return (
        <div className="review-section">
            <div className="review-header">
                {isOnHomePage && (
                    <div className="review-header_star">
                        <img src={LargeStar} />
                        <img src={LargeStar} />
                        <img src={LargeStar} />
                    </div>
                )}
                <div className="review-header_title">
                    <h1>Reviews & Testimonials</h1>
                </div>
                {isOnHomePage && (
                    <div className="review-header_star">
                        <img src={LargeStar} />
                        <img src={LargeStar} />
                        <img src={LargeStar} />
                    </div>
                )}
            </div>
            <div className="review-list">
                <HomePageReviewList reviews={reviews} />
            </div>
        </div>
    );
};

ReviewSection.propTypes = {
    reviews: PropTypes.array,
    isOnHomePage: PropTypes.bool
};

ReviewSection.defaultProps = {
    isOnHomePage: false
};

export default ReviewSection;
