import React from 'react';
import _isEmpty from 'lodash/isEmpty';
import services from '../../../constants/garage-services';
import { Link } from 'react-router-dom';
import './ServiceList.css';

const RenderServiceList = () => {
    const serviceList = [];
    const uniqueList = [];
    for (const item of services) {
        const index = uniqueList.findIndex(it => it.name === item.name);
        if (index === -1) {
            uniqueList.push(item);
        }
    }
    let tempList = [];
    for (const [index, item] of uniqueList.entries()) {
        if (index % 6 === 0) {
            if (!_isEmpty(tempList)) {
                serviceList.push(
                    <div className="col-lg-4 col-md-4">
                        <div className="service-shadow-box-fill service-list">
                            <ul>{tempList}</ul>
                        </div>
                    </div>
                );
                tempList = [];
                tempList.push(
                    <li>
                        <Link to="/garages">{item.name}</Link>
                    </li>
                );
                continue;
            }
        }
        tempList.push(
            <li>
                <Link to="/garages">{item.name}</Link>
            </li>
        );
    }
    if (!_isEmpty(tempList)) {
        serviceList.push(
            <div className="col-lg-4 col-md-4">
                <div className="service-shadow-box-fill service-list">
                    <ul>{tempList}</ul>
                </div>
            </div>
        );
    }
    return serviceList;
};

export default function ServiceList() {
    return (
        <div>
            <div className="row">{RenderServiceList()}</div>
        </div>
    );
}
