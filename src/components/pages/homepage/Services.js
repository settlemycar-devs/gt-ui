import React from 'react';

import './Services.css';

import FullServiceIcon from '../../../asset/icons/service_full.png';
import BrakeServiceIcon from '../../../asset/icons/service_break.png';
import BodyworkServiceIcon from '../../../asset/icons/service_bodywork.png';
import OilChangeServiceIcon from '../../../asset/icons/service_oil_change.png';
import TyreFittingServiceIcon from '../../../asset/icons/service_tyre_fitting.png';
import ClutchServiceIcon from '../../../asset/icons/service_clutch.png';
import CambeltServiceIcon from '../../../asset/icons/service_cambelt.png';
import EngineServiceIcon from '../../../asset/icons/service_engine.png';
import SteeringServiceIcon from '../../../asset/icons/service_steering.png';
import SuspensionServiceIcon from '../../../asset/icons/service_suspension.png';
import navigation from '../../../services/navigation';
import ServiceList1 from './ServiceList';

const services = [
    {
        name: 'Full Service',
        icon: FullServiceIcon,
        id: 1
    },
    {
        id: 2,
        name: 'Brake Pads and Discs',
        icon: BrakeServiceIcon
    },
    {
        id: 3,
        name: 'Bodyworks, Dents and Paints ',
        icon: BodyworkServiceIcon
    },
    {
        id: 4,
        name: 'Oil Change',
        icon: OilChangeServiceIcon
    },
    {
        id: 5,
        name: 'Tyre Fitting',
        icon: TyreFittingServiceIcon
    },
    {
        id: 6,
        name: 'Clutch',
        icon: ClutchServiceIcon
    },
    {
        id: 7,
        name: 'Cambelt',
        icon: CambeltServiceIcon
    },
    {
        id: 8,
        name: 'Engine',
        icon: EngineServiceIcon
    },
    {
        id: 9,
        name: 'Steering',
        icon: SteeringServiceIcon
    },
    {
        id: 10,
        name: 'Suspension',
        icon: SuspensionServiceIcon
    }
];

// export const ServiceList = ({ services }) => {
//     return (
//         <div className="service-list">
//             {services &&
//                 services.map(service => {
//                     return (
//                         <div
//                             key={service.id}
//                             className="service-item-container"
//                             onClick={() => {
//                                 navigation.gotoGarageList();
//                             }}
//                         >
//                             <div className="service-item">
//                                 <img src={service.icon} />
//                                 <div>{service.name}</div>
//                             </div>
//                         </div>
//                     );
//                 })}
//         </div>
//     );
// };

export default function Services() {
    return (
        <div className="services-section">
            <div className="services-header-title">
                <h1>Services</h1>
            </div>
            <div className="service-container">
                <ServiceList1 services={services} />
            </div>
        </div>
    );
}
