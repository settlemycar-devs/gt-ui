import React from 'react';
import { AdminContextProvider } from '../../../contexts/AdminContext';
import Admin from './Admin';

export default function AdminComponent() {
    return (
        <AdminContextProvider>
            <Admin />
        </AdminContextProvider>
    );
}
