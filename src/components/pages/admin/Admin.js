import React, { useContext, useEffect } from 'react';
import { UserContext } from '../../../contexts/UserContext';
import isEmpty from 'lodash/isEmpty';
import { AdminContext } from '../../../contexts/AdminContext';
import map from 'lodash/map';
import QuoteRequestItemAdmin from './QuoteRequestItemAdmin';
import { TopHeader } from '../../header';
import './Admin.css';
import get from 'lodash/get';
import Filter from './Filter';

const AdminHeader = ({ mobile }) => {
    return (
        <div className="admin-header">
            <div>GT Admin</div>
            <div>Mobile No: {mobile}</div>
        </div>
    );
};

export default function Admin() {
    const {
        userContext: { user }
    } = useContext(UserContext);
    const {
        adminContext: { totalData, meta },
        fetchAllQuotes,
        handleLoadMore
    } = useContext(AdminContext);
    useEffect(() => {
        fetchAllQuotes();
    }, []);
    if (isEmpty(user) || !user.roles || !user.roles.includes('admin')) {
        return null;
    }
    return (
        <div className="admin-page">
            <TopHeader />
            <AdminHeader mobile={get(user, 'phone')} />
            <Filter />
            <div className="admin-quotes-container">
                {map(totalData, quoteRequest => {
                    return (
                        <QuoteRequestItemAdmin
                            key={quoteRequest._id}
                            quoteRequest={quoteRequest}
                        />
                    );
                })}
            </div>
            {meta.has_next && (
                <div className={'admin-load-more'} onClick={handleLoadMore}>
                    LOAD MORE
                </div>
            )}
        </div>
    );
}
