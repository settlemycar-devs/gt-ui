import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import APIService from '../../../services/http';
import DayJS from 'dayjs';

import './QuoteRequestItemAdmin.css';
import navigation from '../../../services/navigation';
import DefaultGarageProfile from '../../../asset/images/garage_profile_1.png';
import { RenderPriceDescription } from '../user-profile/QuoteRequestItem';
import get from 'lodash/get';

const Item = ({ title, value }) => {
    return (
        <div className="item-container">
            <div className="title">{title}</div>
            <div className="value">{value}</div>
        </div>
    );
};

export default function QuoteRequestItemAdmin({ quoteRequest }) {
    const [estimatedPrice, setEstimatedPrice] = useState(0);
    const [quoteTags, setQuoteTags] = useState('');
    const [requestSentSuccess, setRequestSentSuccess] = useState(false);
    const [quoteSentSuccess, setQuoteSentSuccess] = useState(false);

    const handleSubmitQuoteSent = e => {
        e.preventDefault();
        if (!estimatedPrice) {
            return;
        }
        const quoteTagsList = quoteTags.split(':');
        APIService.updateQuoteRequest(quoteRequest._id, {
            status: 'quote_sent',
            quote_detail: {
                estimated_price: estimatedPrice,
                quote_tags: quoteTagsList
            },
            communicate: true
        })
            .then(response => {
                console.log('response', response);
                setQuoteSentSuccess(true);
            })
            .catch(error => {
                console.log(error);
            });
    };

    const status = _get(quoteRequest, 'status', '');

    const dayJs = DayJS(_get(quoteRequest, 'created_at'));
    const formattedDate = dayJs.format('DD/MM/YYYY hh:mm a');

    const userName = get(quoteRequest, 'requested_by.name');
    const userMobile = get(quoteRequest, 'requested_by.mobile');
    const userEmail = get(quoteRequest, 'requested_by.email');
    const userId = get(quoteRequest, 'requested_by.id');

    // const requestSentToGarge = get(
    //     quoteRequest,
    //     'communication.request_sent_to_garage_via_mail',
    //     true
    // );
    // const quoteSentToUser = get(
    //     quoteRequest,
    //     'communication.quote_sent_to_user_via_mail',
    //     false
    // );
    // const isUserAccepted = get(
    //     quoteRequest,
    //     'communication.is_user_accepted_quote',
    //     false
    // );

    return (
        <div className="admin-quote-request-item">
            <div className="admin-quote-request-item-container">
                {/* Garage Section */}
                <div
                    className="admin-quote-request-garage"
                    onClick={() => {
                        navigation.gotoGarageProfile(
                            _get(quoteRequest, 'garage_id')
                        );
                    }}
                >
                    <div className="admin-quote-request-garage-img">
                        <img
                            src={
                                _get(quoteRequest, 'garage_detail.avatar') ||
                                DefaultGarageProfile
                            }
                        />
                    </div>
                    <div className="admin-quote-request-garage-name">
                        {_get(quoteRequest, 'garage_detail.name')}
                    </div>
                    <div className="admin-quote-request-garage-address">
                        {_get(quoteRequest, 'garage_detail.address')}
                    </div>
                </div>

                <div className="vertical-line"></div>

                {/* Request Meta & Details */}
                <div className="admin-quote-request-detail">
                    {/* Request Meta */}
                    <div className="admin-quote-request-detail-header">
                        <div className="admin-quote-request-detail-header-date">
                            REQUEST ID:{' '}
                            <span>{_get(quoteRequest, 'request_id')}</span>
                        </div>
                        <div className="admin-quote-request-detail-header-date">
                            Requested ON: <span>{formattedDate}</span>
                        </div>
                        <div className="admin-quote-request-detail-header-status">
                            Status: <span>{status.toUpperCase()}</span>
                        </div>
                    </div>
                    <div>
                        Service:{' '}
                        {_get(quoteRequest, 'request_detail.service_name')}
                    </div>

                    {/* Request Details */}
                    <div className="car-detail">
                        <div className="car-detail-header">
                            <div>Car Brand</div>
                            <div>Model</div>
                            <div>Make</div>
                        </div>
                        <div className="car-detail-value">
                            <div>
                                {_get(
                                    quoteRequest,
                                    'request_detail.brand_name'
                                )}
                            </div>
                            <div>
                                {_get(quoteRequest, 'request_detail.model')}
                            </div>
                            <div>
                                {_get(quoteRequest, 'request_detail.year')}
                            </div>
                        </div>
                    </div>
                    <div className="admin-quote-request-description">
                        {_get(quoteRequest, 'request_detail.other_details')}
                    </div>

                    {/* Requested by */}
                    <div className="requested-by">
                        <div className="requested-by-header">Requested By</div>
                        <Item title={'Name'} value={userName} />
                        <Item title={'Mobile'} value={userMobile} />
                        <Item title={'email'} value={userEmail} />
                        <Item title={'ID'} value={userId} />
                    </div>

                    {status === 'completed' && (
                        <div className="admin-quote-request-detail-fare">
                            Quote Received:{' '}
                            <span>
                                ₹{' '}
                                {_get(
                                    quoteRequest,
                                    'quote_detail.estimated_price'
                                )}
                            </span>
                        </div>
                    )}
                </div>

                {/* Quote Details */}
                {status === 'quote_sent' && (
                    <>
                        <div className="vertical-line"></div>
                        <div className="admin-quote-detail">
                            <div className="">Quote Details</div>
                            <div className="admin-quote-price-detail">
                                <div className="admin-quote-price-title">
                                    Estimated Price
                                </div>
                                <div className="admin-quote-price-value">
                                    ₹{' '}
                                    {_get(
                                        quoteRequest,
                                        'quote_detail.estimated_price'
                                    )}
                                </div>
                            </div>
                            <div>Tags Provided:</div>
                            <RenderPriceDescription
                                quoteRequest={quoteRequest}
                            />
                        </div>
                    </>
                )}

                {/* Email Information */}
                {/* <div className="requested-by">
                    <div className="requested-by-header">Communcation</div>
                    <Item
                        title={'Request sent go garage:'}
                        value={requestSentToGarge ? 'Yes' : 'No'}
                    />
                    <Item
                        title={'Quote sent to user:'}
                        value={quoteSentToUser ? 'Yes' : 'No'}
                    />
                    <Item
                        title={'User accepted quote:'}
                        value={isUserAccepted ? 'Yes' : 'No'}
                    />
                </div> */}
                {status === 'created' && !requestSentSuccess && (
                    <div>
                        <div
                            className="form-search-btn"
                            onClick={() => {
                                APIService.updateQuoteRequest(
                                    quoteRequest._id,
                                    {
                                        status: 'request_sent',
                                        communicate: true
                                    }
                                )
                                    .then(response => {
                                        console.log('response', response);
                                        setRequestSentSuccess(true);
                                    })
                                    .catch(error => {
                                        console.log('error', error);
                                    });
                            }}
                        >
                            Send Request to Garage
                        </div>
                    </div>
                )}
                {status === 'request_sent' && !quoteSentSuccess && (
                    <div className="request-sent-form">
                        <div className="requested-by-header">
                            Send Quote To User
                        </div>
                        <form onSubmit={handleSubmitQuoteSent}>
                            <label>Estimated Quote:</label>
                            <input
                                className="gt-input"
                                type="text"
                                inputMode="numeric"
                                value={estimatedPrice}
                                placeholder="Enter Estimate"
                                onChange={e =>
                                    setEstimatedPrice(e.target.value)
                                }
                            />
                            <label>Tags - </label>
                            <label>
                                Separate with <b>:</b>
                            </label>
                            <input
                                className="gt-input"
                                type="text"
                                placeholder="Tags"
                                value={quoteTags}
                                onChange={e => setQuoteTags(e.target.value)}
                            />
                            <button className="form-search-btn" type="submit">
                                Send Quote
                            </button>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );
}

QuoteRequestItemAdmin.propTypes = {
    quoteRequest: PropTypes.instanceOf(Object)
};
