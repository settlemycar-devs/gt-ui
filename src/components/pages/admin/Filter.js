import React, { useContext } from 'react';
import { Formik, Form } from 'formik';
import GTSelect from '../../search/GTSelect';
import './Admin.css';
import { AdminContext } from '../../../contexts/AdminContext';

const statusOptions = [
    { label: 'CREATED', value: 'created' },
    { label: 'REQUEST_SENT', value: 'request_sent' },
    { label: 'QUOTE_SENT', value: 'quote_sent' },
    { label: 'COMPLETED', value: 'completed' }
];

export default function Filter() {
    const { updateCurrentFilter } = useContext(AdminContext);
    const handleSubmit = values => {
        updateCurrentFilter(values);
    };

    const handleSelectChange = (onChange, name, selectedValue) => {
        onChange(name, selectedValue);
    };

    return (
        <div className="admin-quote-filter">
            <div className="admin-form-container">
                <div style={{ fontWeight: '700', fontSize: '20px' }}>
                    Filter Requests
                </div>
                <Formik
                    onSubmit={handleSubmit}
                    initialValues={{
                        status: ''
                    }}
                    // validate={this.validateForm}
                    validateOnChange={false}
                    validateOnBlur={false}
                >
                    {({
                        values,
                        setFieldValue,
                        setFieldTouched,
                        errors,
                        touched,
                        setErrors
                    }) => (
                        <Form className="admin-form">
                            <div className="admin-form-item select">
                                <GTSelect
                                    name="status"
                                    value={values.status}
                                    onChange={(key, value) => {
                                        setErrors({});
                                        setFieldValue(key, value);
                                    }}
                                    handleOnChange={handleSelectChange}
                                    onBlur={setFieldTouched}
                                    error={errors.brandName}
                                    touched={touched.brandName}
                                    options={statusOptions}
                                    placeholder={'Status'}
                                    classNamePrefix={'gt-react-select'}
                                />
                            </div>
                            <div className="admin-form-item">
                                <button
                                    type="submit"
                                    className="form-search-btn"
                                >
                                    Submit
                                </button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
}
