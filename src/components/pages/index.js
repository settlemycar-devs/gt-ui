export { default as HomePage } from './homepage';
export { default as About } from './about/About';
export { default as Services } from './services/Services';
export { default as GarageSearch } from './garage-search';
export { default as GarageProfile } from './garage-profile';
export { default as BootStrap } from './BootStrap';
export { default as Admin } from './admin';
