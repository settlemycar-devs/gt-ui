import React from 'react';
import { TopHeader } from '../../header';
import { Footer } from '../../footer';

import './PreviousQuote.css';

export default function PreviousQuote() {
    return (
        <div className="up-previous-quote">
            <TopHeader />
            Previous Quote
            <Footer />
        </div>
    );
}
