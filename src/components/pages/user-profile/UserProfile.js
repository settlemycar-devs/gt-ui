import React, { useContext, useEffect } from 'react';
import { TopHeader } from '../../header';
import { Footer } from '../../footer';

import './UserProfile.css';
import UserProfileForm from './UserProfileForm';
import { UserContext } from '../../../contexts/UserContext';
import OnGoingRequest from './OnGoingRequest';
import navigation from '../../../services/navigation';

export default function UserProfile() {
    const {
        userContext: { user },
        isLoggedIn,
        fetchAndSetQuoteRequest,
        setShowLoginModal
    } = useContext(UserContext);

    useEffect(() => {
        if (!isLoggedIn()) {
            navigation.gotoHome();
            setShowLoginModal(true);
            return;
        }
        fetchAndSetQuoteRequest();
    }, []);

    if (!isLoggedIn()) {
        return null;
    }

    return (
        <div className="user-profile">
            <TopHeader />
            <div className="user-profile-header">
                <h2 className="filter-header_h2">Hey, Update Your Profile </h2>
            </div>
            <div className="user-profile-form-container">
                <div className="user-profile-phone-bar">
                    Mobile Number: &nbsp;&nbsp;
                    <span>{user.phone}</span>
                </div>
                <UserProfileForm />
            </div>
            <div className="gp-facilitiies-header">
                <h1 className="gp-facility_h1">Requested Quotes</h1>
            </div>
            <OnGoingRequest />
            <Footer />
        </div>
    );
}
