import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import _map from 'lodash/map';
import GreenTick from '../../../asset/icons/tick_2.svg';
import APIService from '../../../services/http';
import DefaultGarageProfile from '../../../asset/images/garage_profile_1.png';

import './QuoteRequestItem.css';
import navigation from '../../../services/navigation';

export const RenderPriceDescription = ({ quoteRequest }) => {
    const descriptionTags = _get(quoteRequest, 'quote_detail.quote_tags');
    // const descriptionTags = [
    //     'Car Wash',
    //     'Anti Viral & Bacterial Treatment',
    //     'Interior Vacuum Cleaning',
    //     'Dashboard Policing',
    //     'Interior Wet Shampooing & Detailing'
    // ];
    if (_isEmpty(descriptionTags)) {
        return null;
    }
    return (
        <div className="quote-description-tag-list">
            {_map(descriptionTags, (tag, index) => {
                return (
                    <div className="price-description-tags" key={index}>
                        <span className="price-description-tags__icon">
                            <img src={GreenTick} />
                        </span>
                        <span className="price-description-tags__title">
                            {tag}
                        </span>
                    </div>
                );
            })}
        </div>
    );
};

export default function QuoteRequestItem({ quoteRequest }) {
    const [isQuoteAccepted, setIsQuoteAccepted] = useState(false);
    const [isAcceptingQuote, setIsAcceptingQuote] = useState(false);

    const createdAt = new Date(_get(quoteRequest, 'created_at'));
    const options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };

    const formattedCreatedAt = createdAt.toLocaleDateString('en-US', options);
    const status = _get(quoteRequest, 'status', '');

    const handleAcceptQuoteRequest = async () => {
        if (isAcceptingQuote) {
            return;
        }
        try {
            setIsAcceptingQuote(true);
            const response = await APIService.acceptQuoteRequest(
                _get(quoteRequest, '_id')
            );
            console.log('response', response);
            setIsAcceptingQuote(false);
            setIsQuoteAccepted(true);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className="quote-request-item">
            <div className="quote-request-item-container">
                <div
                    className="quote-request-garage"
                    onClick={() => {
                        navigation.gotoGarageProfile(
                            _get(quoteRequest, 'garage_id')
                        );
                    }}
                >
                    <div className="quote-request-garage-img">
                        <img
                            src={
                                _get(quoteRequest, 'garage_detail.avatar') ||
                                DefaultGarageProfile
                            }
                        />
                    </div>
                    <div className="quote-request-garage-name">
                        {_get(quoteRequest, 'garage_detail.name')}
                    </div>
                    <div className="quote-request-garage-address">
                        {_get(quoteRequest, 'garage_detail.address')}
                    </div>
                </div>
                <div className="vertical-line"></div>
                <div className="quote-request-detail">
                    <div className="quote-request-detail-header">
                        <div className="quote-request-detail-header-date">
                            REQUEST ID:{' '}
                            <span>{_get(quoteRequest, 'request_id')}</span>
                        </div>
                        <div className="quote-request-detail-header-date">
                            Requested ON: <span>{formattedCreatedAt}</span>
                        </div>
                        <div className="quote-request-detail-header-status">
                            Status:{' '}
                            <span>
                                {status === 'created'
                                    ? 'ONGOING'
                                    : status.toUpperCase()}
                            </span>
                        </div>
                    </div>
                    <div className="car-detail">
                        <div className="car-detail-header">
                            <div>Car Brand</div>
                            <div>Model</div>
                            <div>Make</div>
                        </div>
                        <div className="car-detail-value">
                            <div>
                                {_get(
                                    quoteRequest,
                                    'request_detail.brand_name'
                                )}
                            </div>
                            <div>
                                {_get(quoteRequest, 'request_detail.model')}
                            </div>
                            <div>
                                {_get(quoteRequest, 'request_detail.year')}
                            </div>
                        </div>
                    </div>
                    <div className="quote-request-description">
                        {_get(quoteRequest, 'request_detail.other_details')}
                    </div>
                    {status === 'completed' && (
                        <div className="quote-request-detail-fare">
                            Quote Received:{' '}
                            <span>
                                ₹{' '}
                                {_get(
                                    quoteRequest,
                                    'quote_detail.estimated_price'
                                )}
                            </span>
                        </div>
                    )}
                </div>
                {status === 'quote_sent' && (
                    <>
                        <div className="vertical-line"></div>
                        <div className="quote-detail">
                            <div className="">Quote Details</div>
                            <div className="quote-price-detail">
                                <div className="quote-price-title">
                                    Estimated Price<sup>*</sup>
                                </div>
                                <div className="quote-price-value">
                                    ₹{' '}
                                    {_get(
                                        quoteRequest,
                                        'quote_detail.estimated_price'
                                    )}
                                </div>
                            </div>
                            {!_isEmpty(
                                _get(quoteRequest, 'quote_detail.quote_tags')
                            ) && (
                                <div className="quote-price-description">
                                    <div className="quote-price-description-header">
                                        It Included
                                    </div>
                                    <RenderPriceDescription
                                        quoteRequest={quoteRequest}
                                    />
                                </div>
                            )}
                            <div className="accept-btn-container">
                                {isQuoteAccepted && (
                                    <div>
                                        Successfully Accepted Quote, Gargage
                                        will contact you soon
                                    </div>
                                )}
                                {!isQuoteAccepted && (
                                    <div
                                        className="form-search-btn btn-see-garages"
                                        onClick={handleAcceptQuoteRequest}
                                    >
                                        Accept Request
                                    </div>
                                )}
                            </div>
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}

QuoteRequestItem.propTypes = {
    quoteRequest: PropTypes.instanceOf(Object)
};
