import React, { useContext } from 'react';
import { UserContext } from '../../../contexts/UserContext';
import _isEmpty from 'lodash/isEmpty';
import _map from 'lodash/map';
import Navigation from '../../../services/navigation';
import QuoteRequestItem from './QuoteRequestItem';

import './OnGoingRequest.css';

export default function OnGoingRequest() {
    const {
        userContext: { isQuoteRequestLoading, quoteRequests }
    } = useContext(UserContext);
    if (isQuoteRequestLoading) {
        return null;
    }
    const onGoingQuoteRequest = quoteRequests.filter(request => {
        return ['confirmed', 'created', 'quote_sent'].includes(request.status);
    });

    const completedQuoteRequest = quoteRequests.filter(request => {
        return ['completed'].includes(request.status);
    });

    if (_isEmpty(quoteRequests)) {
        return (
            <div className="ongoing-quote-empty-container">
                <div className="no-quote-header">
                    You don't have any ongoing Quote Request
                </div>
                <div className="no-quote-subheader">
                    Request Quote from nearby garages
                </div>
                <div>
                    <div
                        className="form-search-btn btn-see-garages"
                        onClick={() => {
                            Navigation.gotoGarageList();
                        }}
                    >
                        See Garages
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="quote-history">
            <div className="ongoing-quote-header">
                <h2 className="quote-request-header_h2">
                    OnGoing Quote Request
                </h2>
            </div>
            {_map(onGoingQuoteRequest, quoteRequest => {
                return (
                    <QuoteRequestItem
                        quoteRequest={quoteRequest}
                        key={quoteRequest._id}
                    />
                );
            })}

            <div className="completed-quote-header">
                <h2 className="quote-request-header_h2">
                    Completed Quote Request
                </h2>
            </div>
            {_map(completedQuoteRequest, quoteRequest => {
                return (
                    <QuoteRequestItem
                        quoteRequest={quoteRequest}
                        key={quoteRequest._id}
                    />
                );
            })}
        </div>
    );
}
