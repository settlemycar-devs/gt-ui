import React, { useContext, useState } from 'react';
import _get from 'lodash/get';
import { Formik, Form, Field } from 'formik';
import { UserContext } from '../../../contexts/UserContext';
import APIService from '../../../services/http';

export default function UserProfileForm() {
    const {
        userContext: { user },
        setUserDetail
    } = useContext(UserContext);

    const [isFormSubmitting, setIsFormSubmitting] = useState(false);
    const [isUpdateSuccess, setIsUpdateSuccess] = useState(false);
    const handleSubmit = async data => {
        if (isFormSubmitting) {
            return;
        }

        if (
            data.name === _get(user, 'profile.name') &&
            data.email === user.email
        ) {
            return;
        }
        setIsFormSubmitting(true);
        try {
            const response = await APIService.updateProfile(data);
            setIsFormSubmitting(false);
            setIsUpdateSuccess(true);
            setUserDetail(response.data);
        } catch (error) {
            setIsFormSubmitting(false);
        }
    };
    return (
        <div className="search-form-container">
            <Formik
                onSubmit={handleSubmit}
                initialValues={{
                    name: _get(user, 'profile.name') || '',
                    email: user.email || ''
                }}
            >
                {({ values, setFieldValue }) => (
                    <Form>
                        <div className="form-item">
                            <label>Name</label>
                            <Field
                                name="name"
                                placeholder={'Name'}
                                onChange={e =>
                                    setFieldValue('name', e.target.value)
                                }
                            />
                        </div>
                        <div className="form-item">
                            <label>Email</label>
                            <Field
                                name="email"
                                placeholder={'Email'}
                                onChange={e =>
                                    setFieldValue('email', e.target.value)
                                }
                            />
                        </div>
                        {isUpdateSuccess && (
                            <div className="form-item">
                                Profile Updated Successfully
                            </div>
                        )}
                        <div className="form-item">
                            <button
                                type="submit"
                                className={`form-search-btn ${
                                    isFormSubmitting ? 'disabled' : ''
                                }`}
                            >
                                Update Profile
                            </button>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    );
}
