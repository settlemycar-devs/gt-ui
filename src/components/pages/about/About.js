import React, { Component } from 'react';
import { TopHeader } from '../../header';
import { Footer } from '../../footer';
import ContactUsForm from './ContactUsForm';

import './About.css';

export default class About extends Component {
    render() {
        return (
            <div className="about-section">
                <TopHeader />
                <div className="about-us">
                    <div className="about-us-inner">
                        <div className="about-us-content">
                            <h3>About GarageTroop and Our Mission</h3>
                            <div className="about-us-content-item1">
                                We are the team of problem solvers more than
                                anything. We observed car owners going through a
                                lot of problems when it comes to repair &
                                services for their car. Although the world with
                                the internet becomes heaven for everyone but
                                when it comes to car repair and services, there
                                is still a void of information. There isn’t any
                                direct information which can help car owners in
                                any way. They are left with uncertainty about
                                pricing, services required, garage ability etc,
                                in their mind. They are getting played by
                                dealers/workshops/mechanics till now. And that’s
                                where we wanted to mark our footprint. This is
                                not going to happen anymore.
                            </div>
                            <div className="about-us-content-item2"></div>
                            <div className="about-us-content-item3">
                                Our aim is to enable great communication between
                                workshops and car owners where there is no place
                                of uncertainty and confusion. We want to help
                                car owners to become Empowered from Helpless.
                                Garage Troop brings you the one-stop-shop to
                                handpicked top garages in your city that you can
                                trust on for the best service. No malpractices
                                on the garage side. No cost inflation. That’s
                                our guarantee. . India’s first garages and car
                                services comparison platform. Finally, you don’t
                                need to worry about the quality of repair,
                                garage’s reputation, Hygiene practises,
                                experience of the repair person, or overpricing
                                by garages. Visit garages now.
                            </div>
                        </div>
                        <div className="about-us-image">
                            <video controls style={{ width: '100%' }}>
                                <source
                                    src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/gt_intro.mp4"
                                    type="video/mp4"
                                />
                                Sorry, your browser doesn't support embedded
                                videos.
                            </video>
                            {/* <img src="https://garagetroop-test.s3-us-west-2.amazonaws.com/common/about-us.jpg" /> */}
                        </div>
                    </div>
                    <div className="contact-us">
                        <div className="gp-facilitiies-header">
                            <h1 className="gp-facility_h1">Contact Us</h1>
                        </div>
                        <div className="contact-us-form-wrapper">
                            <ContactUsForm />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}
