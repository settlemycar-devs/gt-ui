import React, { useState } from 'react';

export default function ContactUsForm() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [message, setMessage] = useState('');
    const [error, setError] = useState('');
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const handleSubmit = e => {
        e.preventDefault();
        if (!email) {
            setError('Email is required');
            return;
        }
        if (!message) {
            setError('Message is required');
        }
        const data = {
            name,
            email,
            phone,
            message
        };
        console.log('data got', data);
        // TODO:@send this form to the server.
        setIsFormSubmitted(true);
    };

    if (isFormSubmitted) {
        return (
            <div className="contact-succes">
                Thank You! Your query is recorded Successfull, We will get back to you soon
                on {email || phone}
            </div>
        );
    }

    return (
        <div className="contact-us-form">
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        className="form-control form-control-lg"
                        placeholder="Enter name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input
                        type="email"
                        className="form-control form-control-lg"
                        id="email"
                        name="email"
                        aria-describedby="emailHelp"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <small id="emailHelp" className="form-text text-muted">
                        We will never share your email with anyone else.
                    </small>
                </div>
                <div className="form-group">
                    <label htmlFor="phone">Phone Number</label>
                    <input
                        type="text"
                        id="phone"
                        name="phone"
                        className="form-control form-control-lg"
                        placeholder="Enter phone"
                        value={phone}
                        onChange={e => setPhone(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Message</label>
                    <textarea
                        className="form-control form-control-lg"
                        id="message"
                        name="message"
                        value={message}
                        onChange={e => setMessage(e.target.value)}
                    ></textarea>
                </div>
                <div style={{ color: 'red', marginBottom: '10px' }}>
                    {error}
                </div>
                <button type="submit" className="btn btn-danger">
                    Submit
                </button>
            </form>
        </div>
    );
}
