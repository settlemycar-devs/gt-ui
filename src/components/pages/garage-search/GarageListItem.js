import React, { useContext } from 'react';
import _get from 'lodash/get';
import _map from 'lodash/map';
import PropTypes from 'prop-types';
import './GarageList.css';
import SmallStar from '../../../asset/icons/star_selected.svg';
import GreenTick from '../../../asset/icons/tick_2.svg';
// import WhiteCheckCirle from '../../../asset/icons/white_circle_check.svg';
import { GarageSearchContext } from '../../../contexts/GarageSearchContext';
import { UserContext } from '../../../contexts/UserContext';
import navigation from '../../../services/navigation';
import DefaultGarageProfile from '../../../asset/images/garage_profile_1.png';
import AddressIcon from '../../../asset/icons/address.svg';

const GenerateReviewStar = ({ totalReview }) => {
    const reviewList = [];
    for (let i = 0; i < totalReview; i++) {
        reviewList.push(
            <span className="garage-item-rating-icon" key={i + 1}>
                <img src={SmallStar} />
            </span>
        );
    }
    if (reviewList.length > 0) {
        return reviewList;
    }
    return null;
};

const RenderTags = ({ tags }) => {
    return (
        <div>
            {_map(tags, (tag, index) => {
                return (
                    <div className="garage-tags" key={index}>
                        <span className="garage-tags__tick-icon">
                            <img src={GreenTick} />
                        </span>
                        <span className="garage-tags__title">{tag}</span>
                    </div>
                );
            })}
        </div>
    );
};

export default function GarageListItem({ garage }) {
    const {
        openGarageRequestModal,
        searchContext: { quoteRequestSuccessGarages }
    } = useContext(GarageSearchContext);
    const { isLoggedIn, setShowLoginModal } = useContext(UserContext);
    return (
        <div className="garage-list-item">
            <div className="garage-list-content">
                <div className="row">
                    <div className="col-5">
                        <div className="garage-list-item-img">
                            <img
                                src={garage.avatar || DefaultGarageProfile}
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="col-7">
                        <div className="garage-list-profile-name">
                            {garage.name}
                        </div>
                        <div className="garage-list-item-address">
                            <img src={AddressIcon} />
                            {garage.address}
                        </div>
                        <div className="garage-list-item-review">
                            <GenerateReviewStar
                                totalReview={
                                    _get(garage, 'overall_rating') || 5
                                }
                            />
                        </div>
                        <a
                            className="gt-map-link"
                            href={`https://maps.google.com/?q=${_get(
                                garage,
                                'location.coordinates.1'
                            )},${_get(garage, 'location.coordinates.0')}`}
                            target="_blank"
                        >
                            Show On Map
                        </a>
                        <div className="garage-list-item-job">
                            <span>{garage.no_of_services}</span> Jobs Completed
                        </div>
                        <div className="garage-list-item-response-time">
                            Average Response Time:{' '}
                            <span>
                                {garage.average_response_time
                                    ? `${garage.average_response_time} hours`
                                    : '24 Hours'}
                            </span>
                        </div>
                        <div className="garage-list-item-since">
                            Since {garage.established_since || 1998}
                        </div>
                        <RenderTags tags={_get(garage, 'tags', [])} />
                    </div>
                </div>
                <div className="garage-list-item-intro">
                    {garage.description ||
                        'One of the best garages in your location, view profile to know more'}
                </div>
                <div className="garage-list-item__bottom">
                    <div
                        className="form-search-btn"
                        onClick={() => {
                            navigation.gotoGarageProfile(garage._id);
                        }}
                    >
                        Virtual Visit
                    </div>
                    {quoteRequestSuccessGarages.includes(garage._id) ||
                    garage.has_quote_request ? (
                        <div className="request-success-btn">
                            Request Received{' '}
                            {/* <span>
                                <img src={WhiteCheckCirle} />
                            </span> */}
                            <br />
                            <span>You will be notified very soon</span>
                        </div>
                    ) : (
                        <div
                            className="form-search-btn get-quote"
                            onClick={() => {
                                if (isLoggedIn()) {
                                    openGarageRequestModal(garage._id);
                                } else {
                                    setShowLoginModal(true);
                                }
                            }}
                        >
                            Ask Price
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}

GarageListItem.propTypes = {
    garage: PropTypes.instanceOf(Object).isRequired
};
