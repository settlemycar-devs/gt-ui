import React, { useContext, useEffect } from 'react';

import Spinner from 'react-spinkit';

import _isEmpty from 'lodash/isEmpty';
import _get from 'lodash/get';

import { GarageSearchContext } from '../../../contexts/GarageSearchContext';
import { TopHeader } from '../../header';
import SearchAsideSection from './SearchAsideSection';
import GarageList from './GarageList';

import './GarageSearch.css';

export default function GarageSearch() {
    const { fetchAndSetGarages, garageData } = useContext(GarageSearchContext);
    useEffect(() => {
        fetchAndSetGarages(true);
    }, []);
    return (
        <div className="">
            <TopHeader />
            <div className="garage-search-section">
                <SearchAsideSection />
                <GarageList />
            </div>
            {_isEmpty(_get(garageData, 'totalData')) &&
                !_get(garageData, 'isLoadingGarageData') && (
                    <div className="garage-empty-state">
                        Sorry no garages found, Please Refine Search
                    </div>
                )}
            {_get(garageData, 'isLoadingGarageData') && (
                <div className="garage-loading-state">
                    <Spinner name="ball-spin-fade-loader" color="#1f396c" />
                </div>
            )}
        </div>
    );
}
