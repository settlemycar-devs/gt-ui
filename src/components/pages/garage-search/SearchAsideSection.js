import React from 'react';
// import FilterSearch from './FilterSearch';
import { RefineSearchForm } from '../../search';
import './SearchAside.css';

export default function SearchAsideSection() {
    return (
        <div className="search-aside-section">
            <div className="search-aside-header">
                <h2 className="search-aside-header_h2">Refine Search</h2>
            </div>
            <RefineSearchForm />
            {/* <FilterSearch /> //TODO: don't need in version 1 */}
        </div>
    );
}
