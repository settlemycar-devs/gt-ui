import React, { useContext } from 'react';
import _map from 'lodash/map';
import _get from 'lodash/get';
import { GarageSearchContext } from '../../../contexts/GarageSearchContext';
import GarageListItem from './GarageListItem';

import './GarageList.css';
import _isEmpty from 'lodash/isEmpty';

export default function GarageList() {
    const { garageData, searchContext, handleLoadMoreGarages } = useContext(
        GarageSearchContext
    );

    return (
        <div className="garage-list-section">
            <div className="garage-list-section__header">
                <h1 className="garage-list-heading_h1">
                    Top Garages in{' '}
                    {_get(searchContext, 'placeInfo.description') ||
                        'your location'}
                </h1>
            </div>
            <div className="garage-list">
                {_map(_get(garageData, 'totalData'), garageData => {
                    return (
                        <div
                            className="garage-list-item-wrapper"
                            key={garageData._id}
                        >
                            <GarageListItem garage={garageData} />
                        </div>
                    );
                })}
                {_get(garageData, 'meta.has_next') && (
                    <div className="load-more-section">
                        <div className="form-item">
                            <button
                                className="form-search-btn"
                                onClick={handleLoadMoreGarages}
                            >
                                Load More
                            </button>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
