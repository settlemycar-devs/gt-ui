import React, { Component } from 'react';
import { TopHeader } from '../../header';
import ServiceList from '../homepage/ServiceList';
import Navigation from '../../../services/navigation';

import './Services.css';

import FullServiceIcon from '../../../asset/icons/service_full.png';
import BrakeServiceIcon from '../../../asset/icons/service_break.png';
import BodyworkServiceIcon from '../../../asset/icons/service_bodywork.png';
import OilChangeServiceIcon from '../../../asset/icons/service_oil_change.png';
import TyreFittingServiceIcon from '../../../asset/icons/service_tyre_fitting.png';
import ClutchServiceIcon from '../../../asset/icons/service_clutch.png';
import CambeltServiceIcon from '../../../asset/icons/service_cambelt.png';
import EngineServiceIcon from '../../../asset/icons/service_engine.png';
import SteeringServiceIcon from '../../../asset/icons/service_steering.png';
import SuspensionServiceIcon from '../../../asset/icons/service_suspension.png';

const services = [
    {
        name: 'Full Service',
        icon: FullServiceIcon,
        id: 1
    },
    {
        id: 2,
        name: 'Brake Pads and Discs',
        icon: BrakeServiceIcon
    },
    {
        id: 3,
        name: 'Bodyworks, Dents and Paints ',
        icon: BodyworkServiceIcon
    },
    {
        id: 4,
        name: 'Oil Change',
        icon: OilChangeServiceIcon
    },
    {
        id: 5,
        name: 'Tyre Fitting',
        icon: TyreFittingServiceIcon
    },
    {
        id: 6,
        name: 'Clutch',
        icon: ClutchServiceIcon
    },
    {
        id: 7,
        name: 'Cambelt',
        icon: CambeltServiceIcon
    },
    {
        id: 8,
        name: 'Engine',
        icon: EngineServiceIcon
    },
    {
        id: 9,
        name: 'Steering',
        icon: SteeringServiceIcon
    },
    {
        id: 10,
        name: 'Suspension',
        icon: SuspensionServiceIcon
    }
];

export default class Services extends Component {
    render() {
        return (
            <div className="page-services-section">
                <TopHeader />
                <div className="page-services-header">
                    <h2>GarageTroop Provides following services</h2>
                </div>
                <div className="page-services-section-container">
                    <ServiceList />
                </div>
                <div className="see-garages-btn-container">
                    <div
                        className="form-search-btn btn-see-garages"
                        onClick={() => {
                            Navigation.gotoGarageList();
                        }}
                    >
                        See Garages
                    </div>
                </div>
            </div>
        );
    }
}
