import React, { useEffect, useContext } from 'react';
import Navigation from '../../services/navigation';
import History from '../../services/history';
import { UserContext } from '../../contexts/UserContext';

export default function BootStrap() {
    const { setShowLoginModal, logout } = useContext(UserContext);
    useEffect(() => {
        const queryParams = History.parseQueryParams();
        Navigation.gotoHome();
        if (queryParams.showLogin) {
            logout(true);
            setShowLoginModal(true);
        }
    });
    return <div></div>;
}
