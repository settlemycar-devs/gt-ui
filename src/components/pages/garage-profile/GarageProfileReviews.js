import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import APIService from '../../../services/http';
import _isEmpty from 'lodash/isEmpty';
import ReviewSection from '../../pages/homepage/ReviewSection';

function GarageProfileReview({ garageData }) {
    const [reviews, setReviews] = useState();
    useEffect(() => {
        async function fetchAndSetReviews() {
            try {
                const reviewGetResponse = await APIService.getGarageReviews(
                    garageData._id
                );
                setReviews(reviewGetResponse.data);
            } catch (error) {
                console.log('Error Fetching Garage Reviews', error);
            }
        }
        fetchAndSetReviews();
    }, []);

    if (_isEmpty(reviews)) {
        return null;
    }

    return (
        <div className="gp-reviews">
            <ReviewSection reviews={reviews} />
        </div>
    );
}

GarageProfileReview.propTypes = {
    garageData: PropTypes.instanceOf(Object)
};

export default GarageProfileReview;
