import React, { useContext } from 'react';
import _get from 'lodash/get';
import _map from 'lodash/map';
import PropTypes from 'prop-types';
import SmallStar from '../../../asset/icons/star_selected.svg';
import GreenTick from '../../../asset/icons/tick_2.svg';
import { GarageSearchContext } from '../../../contexts/GarageSearchContext';
import { UserContext } from '../../../contexts/UserContext';
import DefaultGarageProfile from '../../../asset/images/garage_profile_1.png';
import AddressIcon from '../../../asset/icons/address.svg';

import './GarageProfileItem.css';

const GenerateReviewStar = ({ totalReview }) => {
    const reviewList = [];
    for (let i = 0; i < totalReview; i++) {
        reviewList.push(
            <span className="garage-item-rating-icon" key={i + 1}>
                <img src={SmallStar} />
            </span>
        );
    }
    if (reviewList.length > 0) {
        return reviewList;
    }
    return null;
};

const RenderTags = ({ tags }) => {
    return (
        <div>
            {_map(tags, (tag, index) => {
                return (
                    <div className="garage-tags" key={index}>
                        <span className="garage-tags__tick-icon">
                            <img src={GreenTick} />
                        </span>
                        <span className="garage-tags__title">{tag}</span>
                    </div>
                );
            })}
        </div>
    );
};

export default function GarageListItem({ garage }) {
    const {
        searchContext: { requestQuoteSuccesForGarageId },
        openGarageRequestModal
    } = useContext(GarageSearchContext);
    const { isLoggedIn, setShowLoginModal } = useContext(UserContext);

    return (
        <div className="garage-profile-item">
            <div className="garage-profile-content">
                <div className="row">
                    <div className="gp-profile-img-col col-12 col-md-5">
                        <div className="garage-profile-item-img">
                            <img
                                src={garage.avatar || DefaultGarageProfile}
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="gp-item-content-container col-12 col-md-7">
                        <div className="garage-profile-profile-name">
                            {garage.name}
                        </div>
                        <div className="garage-profile-item-address">
                            <img src={AddressIcon} />
                            {garage.address}
                        </div>
                        <div className="garage-profile-item-review">
                            <GenerateReviewStar
                                totalReview={
                                    _get(garage, 'overall_rating') || 5
                                }
                            />
                        </div>
                        <a
                            className="gt-map-link"
                            href={`https://maps.google.com/?q=${_get(
                                garage,
                                'location.coordinates.1'
                            )},${_get(garage, 'location.coordinates.0')}`}
                            target="_blank"
                        >
                            Show On Map
                        </a>
                        {garage.no_of_services > 0 && (
                            <div className="garage-profile-item-job">
                                <span>{garage.no_of_services}</span> Jobs
                                Completed
                            </div>
                        )}
                        <div className="garage-profile-item-response-time">
                            Average Response Time:{' '}
                            <span>
                                {garage.average_response_time
                                    ? `${garage.average_response_time} hours`
                                    : '24 Hours'}
                            </span>
                        </div>
                        {garage.established_since && (
                            <div className="garage-profile-item-since">
                                Established Since{' '}
                                {garage.established_since || 1998}
                            </div>
                        )}
                        <RenderTags tags={_get(garage, 'tags', [])} />

                        {requestQuoteSuccesForGarageId ===
                            _get(garage, '_id') || garage.has_quote_request ? (
                            <div className="request-success-btn profile">
                                Request Received <br />
                                <span>You will be notified very soon</span>
                            </div>
                        ) : (
                            <div className="garage-profile-item__bottom float-right">
                                <div
                                    className="form-search-btn btn-get-quote"
                                    onClick={() => {
                                        if (isLoggedIn()) {
                                            openGarageRequestModal(
                                                _get(garage, '_id')
                                            );
                                        } else {
                                            setShowLoginModal(true);
                                        }
                                    }}
                                >
                                    Ask Price
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div className="garage-profile-item-intro">
                    {garage.description ||
                        'One of the best garages in your location, view profile to know more'}
                </div>
            </div>
        </div>
    );
}

GarageListItem.propTypes = {
    garage: PropTypes.instanceOf(Object).isRequired
};
