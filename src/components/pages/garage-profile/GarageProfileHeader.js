import React from 'react';
import PropTypes from 'prop-types';

import './GarageProfileHeader.css';

import GarageProfileItem from './GarageProfileItem';

function GarageProfileHeader({ garageData }) {
    return (
        <div className="garage-profile-header">
            <GarageProfileItem garage={garageData} />
        </div>
    );
}

GarageProfileHeader.propTypes = {
    garageData: PropTypes.instanceOf(Object)
};

export default GarageProfileHeader;
