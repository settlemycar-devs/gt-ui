import React from 'react';
import PropTypes from 'prop-types';
import _map from 'lodash/map';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

import './GarageProfileGallary.css';

function GarageProfileGallary({ garageData }) {
    return (
        <div className="gp-gallary">
            {_get(garageData, 'media.source') && (
                <>
                    <div className="gp-facilitiies-header">
                        <h1 className="gp-facility_h1">Garage Tour</h1>
                    </div>
                    <div className="gp-gallary-video">
                        <iframe
                            title="garage-tour"
                            src={_get(garageData, 'media.source').replace(
                                'watch?v=',
                                'embed/'
                            )}
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                        ></iframe>
                    </div>
                </>
            )}
            {!_isEmpty(_get(garageData, 'portfolio')) && (
                <>
                    <div className="gp-facilitiies-header">
                        <h1 className="gp-facility_h1">Gallery</h1>
                    </div>
                    <div className="gp-gallary-images">
                        {_map(garageData.portfolio, (imageUrl, index) => {
                            return (
                                <div
                                    className="gp-gallary-images-item"
                                    key={index}
                                >
                                    <div className="gp-gallary-images-item-container">
                                        <img src={imageUrl} />
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </>
            )}
        </div>
    );
}

GarageProfileGallary.propTypes = {
    garageData: PropTypes.instanceOf(Object)
};

export default GarageProfileGallary;
