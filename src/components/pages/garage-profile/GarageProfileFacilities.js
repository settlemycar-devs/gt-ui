import React from 'react';
import PropTypes from 'prop-types';
import _map from 'lodash/map';

import './GarageProfileFacilities.css';

import ParkingIcon from '../../../asset/icons/gt_icon_facility_parking.png';
import CustomerLoungeIcon from '../../../asset/icons/gt_icon_facility_customer_lounge.png';
import AverageMinimumTimeIcon from '../../../asset/icons/gt_icon_facility_average_minimum_time.png';
import PickupDropIcon from '../../../asset/icons/gt_icon_facility_pickup_drop.png';
import WifiIcon from '../../../asset/icons/gt_icon_facility_wifi.png';
import WashRoomIcon from '../../../asset/icons/gt_icon_facility_washroom.png';
import LiftIcon from '../../../asset/icons/gt_icon_numbers_lifts.png';
import PaintBoothIcon from '../../../asset/icons/gt_icon_numbers_paintbooth.png';
import MechanicIcon from '../../../asset/icons/gt_icon_numbers_mechanic.png';
import ElectricianIcon from '../../../asset/icons/gt_icon_numbers_electrician.png';

import GreenCircleCheck from '../../../asset/icons/green_circle_check.svg';
import RedCircleCross from '../../../asset/icons/red_circle_cross.svg';

const facilityIconMap = {
    gt_icon_facility_parking: ParkingIcon,
    gt_icon_facility_customer_lounge: CustomerLoungeIcon,
    gt_icon_facility_average_minimum_time: AverageMinimumTimeIcon,
    gt_icon_facility_pickup_drop: PickupDropIcon,
    gt_icon_facility_wifi: WifiIcon,
    gt_icon_facility_washroom: WashRoomIcon,
    gt_icon_numbers_lifts: LiftIcon,
    gt_icon_numbers_paintbooth: PaintBoothIcon,
    gt_icon_numbers_mechanic: MechanicIcon,
    gt_icon_numbers_electrician: ElectricianIcon
};

function GarageProfileFacilities({ garageData }) {
    return (
        <div className="gp-facilities-section">
            <div className="gp-facilities">
                {_map(garageData.facilities, facility => {
                    const Icon =
                        facility.icon || facilityIconMap[facility.icon_name];
                    return (
                        <div className="gp-facility-item" key={facility.title}>
                            <div className="gp-facility-item-main-section">
                                <div className="facility-img-wrapper">
                                    <img src={Icon} />
                                </div>
                                <div className="gp-facility-title">
                                    {facility.title}
                                </div>
                            </div>
                            <div className="gp-facility-item-tick">
                                {facility.is_available ? (
                                    <img src={GreenCircleCheck} />
                                ) : (
                                    <img src={RedCircleCross} />
                                )}
                            </div>
                        </div>
                    );
                })}
            </div>
            <div className="gp-facilitiies-header">
                <h1 className="gp-facility_h1">Garage In Numbers</h1>
            </div>
            <div className="gp-numbers">
                {_map(garageData.garage_in_numbers, gNumbers => {
                    const Icon =
                        gNumbers.icon || facilityIconMap[gNumbers.icon_name];
                    return (
                        <div className="gp-numbers-item" key={gNumbers.title}>
                            <div className="gp-numbers-item-main-section">
                                <img src={Icon} />
                                <div className="gp-numbers-title">{`${gNumbers.total} ${gNumbers.title}`}</div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

GarageProfileFacilities.propTypes = {
    garageData: PropTypes.instanceOf(Object)
};

export default GarageProfileFacilities;
