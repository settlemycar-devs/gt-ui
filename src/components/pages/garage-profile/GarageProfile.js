import React, { useState, useEffect, useContext } from 'react';
import _get from 'lodash/get';
import Spinner from 'react-spinkit';

import { History } from '../../../services/';
import APIService from '../../../services/http';
import TopHeader from '../../header/TopHeader';
import GarageProfileHeader from './GarageProfileHeader';
import GarageProfileFacilities from './GarageProfileFacilities';
import GarageProfileGallary from './GarageProfileGallary';
import GarageProfileReviews from './GarageProfileReviews';
import GarageProfileFaq from './GarageProfileFaq';
import { GarageSearchContext } from '../../../contexts/GarageSearchContext';
import { UserContext } from '../../../contexts/UserContext';

import './GarageProfile.css';
import { Footer } from '../../footer';

export default function GarageProfile() {
    const [garageData, setGarageData] = useState();
    const [error, setError] = useState();
    const queryParams = History.parseQueryParams();
    const { isLoggedIn, setShowLoginModal } = useContext(UserContext);
    const garageId = _get(queryParams, 'id');

    const {
        searchContext: { requestQuoteSuccesForGarageId },
        openGarageRequestModal
    } = useContext(GarageSearchContext);

    useEffect(() => {
        async function fetchAndSetGarageProfile() {
            try {
                const garageProfileResponse = await APIService.getGarageProfile(
                    garageId
                );
                setGarageData(currentGarageData => {
                    return garageProfileResponse.data;
                });
            } catch (error) {
                setError('Garage Profile not found');
                console.log('error fetching garage profile', error);
            }
        }
        fetchAndSetGarageProfile();
    }, []);

    if (error) {
        return (
            <div className="garage-profile-page">
                <TopHeader />
                <div className="garage-empty-state profile">{error}</div>
            </div>
        );
    }

    if (!garageData) {
        return (
            <div className="garage-profile-page">
                <TopHeader />
                <div className="garage-loading-state profile">
                    <Spinner name="ball-spin-fade-loader" color="#1f396c" />
                </div>
            </div>
        );
    }

    return (
        <div className="garage-profile-page">
            <TopHeader />
            <GarageProfileHeader garageData={garageData} />
            <div
                style={{
                    width: '100%',
                    height: '1px',
                    background: 'rgb(230, 236, 240)'
                }}
            />
            <GarageProfileFacilities garageData={garageData} />
            <GarageProfileGallary garageData={garageData} />
            <GarageProfileReviews garageData={garageData} />
            <GarageProfileFaq garageData={garageData} />
            {requestQuoteSuccesForGarageId === _get(garageData, '_id') && (
                <div
                    style={{
                        display: 'flex',
                        width: '100%',
                        justifyContent: 'center'
                    }}
                >
                    <div className="request-success-btn profile">
                        Request Received <br />
                        <span>You will be notified very soon</span>
                    </div>
                </div>
            )}
            {requestQuoteSuccesForGarageId !== _get(garageData, '_id') &&
                !garageData.has_quote_request && (
                    <div
                        className="gp-get-btn-bottom-container"
                        onClick={() => {
                            if (isLoggedIn()) {
                                openGarageRequestModal(_get(garageData, '_id'));
                            } else {
                                setShowLoginModal(true);
                            }
                        }}
                    >
                        <div className="garage-profile-item__bottom float-right">
                            <div className="form-search-btn btn-get-quote">
                                Ask Price
                            </div>
                        </div>
                    </div>
                )}
            <Footer />
        </div>
    );
}
