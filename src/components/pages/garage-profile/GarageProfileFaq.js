import React from 'react';
import PropTypes from 'prop-types';
import _map from 'lodash/map';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import './GarageProfileFaq.css';

function GarageProfileFaq({ garageData }) {
    if (_isEmpty(garageData.faqs)) {
        return null;
    }
    return (
        <div className="gp-faqs">
            <div className="gp-facilitiies-header">
                <h1 className="gp-facility_h1">FAQ</h1>
            </div>
            <div className="gp-faqs-list">
                {_map(_get(garageData, 'faqs'), faq => {
                    return (
                        <div className="gp-faq-list-item" key={faq._id}>
                            <div className="gp-faq-question">
                                {faq.question}
                            </div>
                            <div className="gp-faq-answer">{faq.answer}</div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

GarageProfileFaq.propTypes = {
    garageData: PropTypes.instanceOf(Object)
};

export default GarageProfileFaq;
