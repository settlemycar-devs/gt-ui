import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form, Field } from 'formik';
import _map from 'lodash/map';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import debounce from 'debounce-promise';
import AsyncSelect from 'react-select/async';

import APIService from '../../services/http';
import GTSelect from '../search/GTSelect';
import GTRadioGroup from '../search/GTRadioGroup';
import { GarageSearchContext } from '../../contexts/GarageSearchContext';
import FormattedOptions from '../shared/FormattedReactSelectItem';
import FormattedServiceOptions from '../shared/FormattedServiceItem';
import servicesList from '../../constants/garage-services';
import locationIcon from '../../asset/icons/location.svg';

import '../search/SearchForm.css';
import './QuoteReviewForm.css';

export default class QuoteReviewForm extends Component {
    static propTypes = {
        prop: PropTypes
    };

    static contextType = GarageSearchContext;
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getPlacesFromSearchTerm = this.getPlacesFromSearchTerm.bind(this);
        this.handleNearMeClick = this.handleNearMeClick.bind(this);
        this.state = {
            modelOptions: [],
            yearOptions: [],
            locationsList: [],
            isLoadingReview: true,
            isEmailRequired: false,
            showModalField: false,
            showVersionField: false,
            showLocationField: false,
            showServiceField: false
        };
    }

    async componentDidMount() {
        const { setShowQuoteReviewModal } = this.context;
        try {
            const responseData = await APIService.reviewGarage();
            if (_get(responseData, 'data.check_email')) {
                this.setState({ isEmailRequired: true });
            }
            const { searchContext } = this.context;
            const data = {
                showModalField: _isEmpty(_get(searchContext, 'brandName')),
                showVersionField: _isEmpty(_get(searchContext, 'version')),
                showLocationField: _isEmpty(_get(searchContext, 'placeInfo')),
                showServiceField: _isEmpty(
                    _get(searchContext, 'selectedService')
                )
            };
            this.setState({ isLoadingReview: false, ...data });
        } catch (error) {
            setShowQuoteReviewModal(false);
            console.log('error occured while review quote request', error);
        }
    }

    handleSubmit() {
        const {
            searchContext: { isRequestingQuote },
            requestQuote
        } = this.context;
        if (isRequestingQuote) {
            return;
        }
        requestQuote();
    }

    handleSelectChange(onChange, name, selectedValue) {
        const {
            searchContext: { brands }
        } = this.context;
        const brand = brands.find(brand => brand._id === selectedValue._id);
        const models = _map(brand.brand_models, model => {
            return {
                label: model,
                value: model
            };
        });
        this.setState({ modelOptions: models });
        onChange(name, selectedValue);
    }

    async getPlacesFromSearchTerm(currentValue) {
        try {
            const response = await APIService.getPlaces(currentValue);
            const places = response.data;
            const locationList = _map(places, place => {
                return {
                    label: place.title,
                    value: place.place_id,
                    description: place.description
                };
            });
            return locationList;
        } catch (error) {
            console.log(error);
            return [];
        }
    }

    loadOptions = inputValue => {
        return new Promise(resolve => {
            this.getPlacesFromSearchTerm(inputValue).then(locationList => {
                this.setState({ defaultLocationOptions: locationList });
                resolve(locationList);
            });
        });
    };

    validateSearchForm = values => {
        const errors = {};
        const { searchContext } = this.context;
        if (this.state.isEmailRequired && _isEmpty(values.email)) {
            errors.location = 'Email Required';
            return errors;
        }
        if (_isEmpty(searchContext.brandName)) {
            errors.brandName = 'Car Brand is required';
            return errors;
        }
        if (_isEmpty(searchContext.model)) {
            errors.model = 'Car model is required';
            return errors;
        }
        if (_isEmpty(searchContext.year)) {
            errors.year = 'Make year is required';
            return errors;
        }
        if (_isEmpty(searchContext.selectedService)) {
            errors.selectedService = 'Please Select Service';
            return errors;
        }
        return errors;
    };

    handleNearMeClick() {
        const { setPlaceInfo, setLocation } = this.context;
        const successCB = currentPosition => {
            const latitude = _get(currentPosition, 'coords.latitude');
            const longitude = _get(currentPosition, 'coords.longitude');
            setPlaceInfo({
                label: 'My Location',
                value: 'My Location'
            });
            setLocation({ latitude, longitude });
            console.log('CurrentPosition:', latitude, longitude);
        };
        const errorCB = error => {
            if (error.code === 1) {
                alert(
                    `You have denied 'GarageTroop' access to your location. Check your browser settings and give permission to see garages around you.`
                );
            }
        };
        if (window.navigator) {
            window.navigator.geolocation.getCurrentPosition(successCB, errorCB);
        }
    }

    render() {
        const {
            setBrandName,
            setModel,
            setYear,
            setOtherDetails,
            setVersion,
            setEmail,
            searchContext,
            setPlaceInfo,
            setSelectedService
        } = this.context;
        const { brands, versionList, isRequestingQuote } = searchContext;

        if (this.state.isLoadingReview) {
            return (
                <div className="quote-review-container">
                    <div className="quote-review-header">
                        One more step please! Please fill out the below details
                        for better quote request
                    </div>
                    <div className="quote-review-loader">Loading...</div>
                </div>
            );
        }
        return (
            <div className="quote-review-container">
                <div className="quote-review-header">
                    One more step please! Please fill out the below details for
                    better quote request
                </div>
                <div className="search-form-container">
                    <Formik
                        onSubmit={this.handleSubmit}
                        initialValues={{
                            brandName: _get(searchContext, 'brandName', ''),
                            model: _get(searchContext, 'model', ''),
                            year: _get(searchContext, 'year', ''),
                            location: _get(searchContext, 'placeInfo'),
                            otherDetails: _get(
                                searchContext,
                                'otherDetails',
                                ''
                            ),
                            version: _get(searchContext, 'version', ''),
                            email: '',
                            selectedService: _isEmpty(
                                _get(searchContext, 'selectedService')
                            )
                                ? ''
                                : _get(searchContext, 'selectedService')
                        }}
                        validate={this.validateSearchForm}
                        validateOnChange={false}
                        validateOnBlur={false}
                    >
                        {({
                            values,
                            setFieldValue,
                            setFieldTouched,
                            errors,
                            touched,
                            setErrors
                        }) => (
                            <Form>
                                {this.state.showModalField && (
                                    <>
                                        <div className="form-item">
                                            <GTSelect
                                                name="brandName"
                                                value={values.brandName}
                                                onChange={(key, value) => {
                                                    setFieldValue(key, value);
                                                    setBrandName(value);
                                                }}
                                                handleOnChange={
                                                    this.handleSelectChange
                                                }
                                                onBlur={setFieldTouched}
                                                error={errors.brandName}
                                                touched={touched.brandName}
                                                options={brands}
                                                getOptionLabel={({ brand }) =>
                                                    brand
                                                }
                                                getOptionValue={({ _id }) =>
                                                    _id
                                                }
                                                placeholder={'Car Brand'}
                                                classNamePrefix={
                                                    'gt-react-select'
                                                }
                                                defaultValue={_get(
                                                    searchContext,
                                                    'brandName',
                                                    ''
                                                )}
                                            />
                                        </div>

                                        <div className="model-year-container">
                                            <div className="form-item">
                                                <GTSelect
                                                    name={'model'}
                                                    value={values.model}
                                                    onChange={(key, value) => {
                                                        setFieldValue(
                                                            key,
                                                            value
                                                        );
                                                        setModel(value);
                                                    }}
                                                    onBlur={setFieldTouched}
                                                    error={errors.model}
                                                    touched={touched.model}
                                                    options={
                                                        this.state.modelOptions
                                                    }
                                                    placeholder={'Model'}
                                                    classNamePrefix={
                                                        'gt-react-select'
                                                    }
                                                />
                                            </div>
                                            <div className="form-item">
                                                <GTSelect
                                                    name={'year'}
                                                    value={values.year}
                                                    onChange={(key, value) => {
                                                        setFieldValue(
                                                            key,
                                                            value
                                                        );
                                                        setYear(value);
                                                    }}
                                                    onBlur={setFieldTouched}
                                                    error={errors.year}
                                                    touched={touched.year}
                                                    options={
                                                        searchContext.makeYears
                                                    }
                                                    placeholder={'Year'}
                                                    classNamePrefix={
                                                        'gt-react-select'
                                                    }
                                                />
                                            </div>
                                        </div>
                                    </>
                                )}
                                {this.state.isEmailRequired && (
                                    <>
                                        <div className="form-item">
                                            <Field
                                                type="email"
                                                name="email"
                                                placeholder="Email"
                                                onChange={e => {
                                                    setFieldValue(
                                                        'email',
                                                        e.target.value
                                                    );
                                                    setEmail(e.target.value);
                                                }}
                                            />
                                        </div>
                                        <div>
                                            We will not share your email with
                                            anyone
                                        </div>
                                    </>
                                )}
                                {this.state.showVersionField && (
                                    <div className="form-item form-item-version">
                                        <label>Version</label>
                                        <GTRadioGroup
                                            radioList={versionList}
                                            handleOnChange={value => {
                                                setVersion(value);
                                            }}
                                        />
                                    </div>
                                )}
                                {this.state.showLocationField && (
                                    <div className="form-item">
                                        <AsyncSelect
                                            name={'location'}
                                            loadOptions={debounce(
                                                this.loadOptions,
                                                1000
                                            )}
                                            cacheOptions
                                            onChange={value => {
                                                setErrors({});
                                                setFieldValue(
                                                    'location',
                                                    value
                                                );
                                                setPlaceInfo(value);
                                            }}
                                            placeholder={'Location'}
                                            classNamePrefix={'gt-react-select'}
                                            formatOptionLabel={FormattedOptions}
                                            defaultOptions={
                                                this.state
                                                    .defaultLocationOptions
                                            }
                                            value={
                                                _isEmpty(
                                                    _get(
                                                        searchContext,
                                                        'placeInfo'
                                                    )
                                                )
                                                    ? ''
                                                    : _get(
                                                          searchContext,
                                                          'placeInfo'
                                                      )
                                            }
                                        />
                                        <div
                                            className="near-me-btn"
                                            onClick={() => {
                                                setFieldValue('location', {
                                                    label: 'My Location',
                                                    value: 'My Location'
                                                });
                                                this.handleNearMeClick();
                                            }}
                                        >
                                            <img
                                                src={locationIcon}
                                                alt="dummy"
                                            />
                                            <span>Near Me</span>
                                        </div>
                                    </div>
                                )}
                                {this.state.showServiceField && (
                                    <div className="form-item">
                                        <GTSelect
                                            name="selectedService"
                                            value={values.selectedService}
                                            onChange={(key, value) => {
                                                setErrors({});
                                                setFieldValue(key, value);
                                                setSelectedService(value);
                                            }}
                                            onBlur={setFieldTouched}
                                            error={errors.selectedService}
                                            touched={touched.selectedService}
                                            options={servicesList}
                                            formatOptionLabel={
                                                FormattedServiceOptions
                                            }
                                            placeholder={'Service'}
                                            classNamePrefix={'gt-react-select'}
                                        />
                                    </div>
                                )}
                                <label className="otherdetails-label">
                                    Please describe your problem in detail
                                </label>
                                <div className="form-item">
                                    <Field
                                        name="otherDetails"
                                        component="textarea"
                                        placeholder="Other Details (Optional)"
                                        onChange={e => {
                                            setFieldValue(
                                                'otherDetails',
                                                e.target.value
                                            );
                                            setOtherDetails(e.target.value);
                                        }}
                                    />
                                </div>
                                <div className="form-validation-errors">
                                    {Object.keys(errors).map((error, index) => {
                                        return (
                                            <li key={index}>{errors[error]}</li>
                                        );
                                    })}
                                </div>
                                <div className="form-item">
                                    <button
                                        type="submit"
                                        className={`form-search-btn ${
                                            isRequestingQuote ? 'disabled' : ''
                                        }`}
                                    >
                                        Request Quote
                                    </button>
                                </div>
                                {/* <pre>{JSON.stringify(values, null, 2)}</pre> */}
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        );
    }
}

QuoteReviewForm.contextType = GarageSearchContext;
