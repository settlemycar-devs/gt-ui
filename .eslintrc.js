module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "react-app", // extends base eslint provided by create-react-app
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:prettier/recommended", // eslint-config-prettier
        "prettier/flowtype",
        "prettier/standard", // this will override all the above rules
        "prettier/react"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "prettier"
    ],
    "rules": {
        "no-console": ["off"],
        "no-useless-catch": ["off"],
        "react-hooks/exhaustive-deps": ["off"],
        "jsx-a11y/alt-text": ["off"],
        "prettier/prettier": [ // prettier config
            "error",
            {
                "tabWidth": 4,
                "singleQuote": true
            }
        ],
    }
};